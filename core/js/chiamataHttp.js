var flight = [
		//["The Architecture and Datasets of Docear's Research Paper Recommender System" , "http://www.dlib.org/dlib/november14/beel/11beel.html"],
		["The Social, Political and Legal Aspects of Text and Data Mining (TDM)" , "http://www.dlib.org/dlib/november14/brook/11brook.html"],
		["Efficient Blocking Method for a Large Scale Citation Matching", "http://www.dlib.org/dlib/november14/fedoryszak/11fedoryszak.html"],
		["Discovering and Visualizing Interdisciplinary Content Classes in Scientific Publications" , "http://www.dlib.org/dlib/november14/giannakopoulos/11giannakopoulos.html"],
		["Annota: Towards Enriching Scientific Publications with Semantics and User Annotations" , "http://www.dlib.org/dlib/november14/holub/11holub.html"],
		["Experiments on Rating Conferences with CORE and DBLP" , "http://www.dlib.org/dlib/november14/jahja/11jahja.html"],
		["A Comparison of Two Unsupervised Table Recognition Methods from Digital Scientific Articles" , "http://www.dlib.org/dlib/november14/klampfl/11klampfl.html"],
		["Towards Semantometrics: A New Semantic Similarity Based Measure for Assessing a Research Publication's Contribution" , "http://www.dlib.org/dlib/november14/knoth/11knoth.html"],
		["Extracting Textual Descriptions of Mathematical Expressions in Scientific Papers" , "http://www.dlib.org/dlib/november14/kristianto/11kristianto.html"],
		["Towards a Marketplace for the Scientific Community: Accessing Knowledge from the Computer Science Domain" , "http://www.dlib.org/dlib/november14/kroell/11kroell.html"],
		["AMI-diagram: Mining Facts from Images" , "http://www.dlib.org/dlib/november14/murray-rust/11murray-rust.html"],
		["The ContentMine Scraping Stack: Literature-scale Content Mining with Community-maintained Collections of Declarative Scrapers" , "http://www.dlib.org/dlib/november14/smith-unna/11smith-unna.html"],
		["GROTOAP2 — The Methodology of Creating a Large Ground Truth Dataset of Scientific Articles" , "http://www.dlib.org/dlib/november14/tkaczyk/11tkaczyk.html"],
		["A Keyquery-Based Classification System for CORE" , "http://www.dlib.org/dlib/november14/voelske/11voelske.html"],
			   //LINK DI STATISTICA
		["Testing normality of latent variables in the polychoric correlation" , "http://rivista-statistica.unibo.it/article/view/4594"],
		["Multivariate normal-Laplace distribution and processes", "http://rivista-statistica.unibo.it/article/view/4595"],
		["On a Test of Hypothesis to Verify the Operating Risk Due to Accountancy Errors" , "http://rivista-statistica.unibo.it/article/view/4597"],
		["A stratified Mangat and Singh’s optional randomized response model using proportional and optimal allocation" , "http://rivista-statistica.unibo.it/article/view/4598"],
		["n concurvity in nonlinear and nonparametric regression models" , "http://rivista-statistica.unibo.it/article/view/4599"],
		["Estimation of finite population variance using auxiliary information in sample surveys" , "http://rivista-statistica.unibo.it/article/view/4600"],
		["Confluent gamma density in modelling tsunami interevent times" , "http://rivista-statistica.unibo.it/article/view/4601"],
			  	//ARTICOLI DI ALMATOURISM
		["Food, Tourism and Health: a Possible Sinergy?" , "http://almatourism.unibo.it/article/view/5290"],
		["Food and Wine Tourism: an Analysis of Italian Typical Products" , "http://almatourism.unibo.it/article/view/5293"],
		["Food and Wine Tourism as a Pull Factor for Tuscany" , "http://almatourism.unibo.it/article/view/5294"],
		["Promoting Gastronomic Tourism to Foster Local Development: The Stakehodelrs’ Perspective" , "http://almatourism.unibo.it/article/view/5292"],
		["The Douro Region: Wine and Tourism" , "http://almatourism.unibo.it/article/view/4647"],
			  //ARTICOLI DI ANTROPOLOGIA E TEATRO
		["Intercultura, pluralismo ed estetica" , "http://antropologiaeteatro.unibo.it/article/view/5295"],
		["A Dream, please! L’antropologia performativa del ventennio berlusconiano (1994-2014)" , "http://antropologiaeteatro.unibo.it/article/view/5296"],
		["Lo spett-attore: il teatro partecipato di Roger Bernat" , "http://antropologiaeteatro.unibo.it/article/view/5297"],
		["Semel in anno licet insanire. Rituali e maschere nelle Alpi Occidentali e il convegno Masks and Metamorphosis (Helsinki, 3-4 ottobre 2014)" , "http://antropologiaeteatro.unibo.it/article/view/5298"],
		["La densità di una transizione. Successione, divinazione e infanticidio nel Ghana nord-orientale" , "http://antropologiaeteatro.unibo.it/article/view/5429"],
			  //ARTICOLI A SCELTA DI UNA ISSUE DI DLIB
		["The HZSK Repository: Implementation, Features, and Use Cases of a Repository for Spoken Language Corpora" , "http://dlib.org/dlib/september14/jettka/09jettka.html"],
		["Exposing Data From an Open Access Repository for Economics As Linked Data" , "http://dlib.org/dlib/september14/latif/09latif.html"],
		["The Role of a Digital Repository in a Library-Managed Open Access Fund Program" , "http://dlib.org/dlib/september14/zuniga/09zuniga.html"],
		["Library of Congress Recommended Format Specifications: Encouraging Preservation Without Discouraging Creation" , "http://dlib.org/dlib/september14/westervelt/09westervelt.html"],
		["Testing the HathiTrust Copyright Search Protocol in Germany: A Pilot Project on Procedures and Resources" , "http://dlib.org/dlib/september14/behnk/09behnk.html"],
		["Connecting Systems for Better Services Around Special Collections" , "http://dlib.org/dlib/september14/vanbergen/09vanbergen.html"]
];

/* 
* FUNZIONE CHE GENERA LA LISTA DEI DOCUMENTI 
*/
$(function(){
	var html="<ol id='doc_list_din'>";
	for (i=0; i < 36; i++){
		//console.log(flight[i][1]);
		html+='<li><a onclick=\"do_the_click(name)\" href=\"javascript:void(0);\" name=\"'+flight[i][1]+'\">'+flight[i][0]+'</a></li>';
	}
	document.getElementById('doc').innerHTML+=html;
})
/* 
* AUTOMATIZZA LA CHIAMATA ALLE FUNZIONI AJAX PER LA VISUALIZZAZIONE DEI DOCUMENTI E CHIAMATA ALLO SCRAPER 
*/
function do_the_click(url){
	url_di_passaggio=url;
	

	//istruzioni per gestire il cambiamento di documento
	if(tutto_salvato==1) {
		$("#modale_cambiamento_documento").modal('show');

		
	}
	else {
		visualizza_documento_goutte(url_di_passaggio);
	}
}
/* 
* MA SI DAI CI METTO ANCHE LA FUNZIONE DELLE LABEL DATO CHE NON SO COME MAI SE LA METTO IN 
* UN ALTRO FILE LISTA_JSON[ULTIMO] DIVENTA UNDEFINED
* Allora faccio tutte le casistiche basandomi sul nostro json, entro in profondita' in base all'annotazione, la citazione e' un bordello
*/
function visualizza_nostre_label(json,identificatore,sav){
	//$("div#sidr2").html(""); //questa istruzione ad ogni nuovo documento svuota tutti div con classe minicard e carica i nuovi e non mette nulla in append
	//console.log(JSON.stringify(lista_json[ultimo].annotations[0].label));
	//$("div#sidr2").html(JSON.stringify(lista_json[ultimo].annotations[0].label));
	//console.log("Sono in visualizza_nostre_label");
	//console.log(JSON.stringify(json.body.resource.annotations[j][[0]]));
	//non crea problemi, ma ho notato che ogni li aggiunto fa parte di un ul diverso: ci sono tanti ul con un solo li e non un unico ul con tanti li
		var frase;
		if(sav==0) {
			frase="Salvata";
		}
		else frase="Non salvata";

		var ul = $("<ul>");
	
		//console.log(i);
		//ul.append("<li><div id='label'>"+JSON.stringify(json.label)+":"+JSON.stringify(json.body.object)+"</div></li>");
		if(json.type == "hasAuthor"){
			var title = JSON.stringify(json.label);
			var content = JSON.stringify(json.body.label);

			title=rimuovi_backslash(title);
			content=rimuovi_backslash(content);
			//annotazioni.push(json.body.label);
			//console.log(json.body.label);
			ul.append("<li><div class='minicard'><div class='minicard-head'>"+title+"</div><div class='minicard-body'><span class='labello'>"+content+"</span><br><div id='modann'><button type='button' onclick='modifica_annotazione("+identificatore+")' class='btn btn-primary btn-xs modifica_ann num"+identificatore+"'><span id='modann'>modify</span></button>  <button type='button' onclick='cancella_annotazione("+identificatore+")' class='btn btn-primary btn-xs cancella_ann num"+identificatore+"'><span id='modann'>delete</span></button></div><p class='fraseee'>"+frase+"</p></div></li>");
		}
		if(json.type == "hasTitle" || json.type == "hasDOI" || json.type == "hasURL" || json.type == "hasPublicationYear" || json.type == "denotesRhetoric" || json.type == "hasComment"){ //il commento non sappiamo se e' giusto se da errori si cava	
			var title = JSON.stringify(json.label);
			var content = JSON.stringify(json.body.object);

			title=rimuovi_backslash(title);
			content=rimuovi_backslash(content);
			ul.append("<li><div class='minicard'><div class='minicard-head'>"+title+"</div><div class='minicard-body'><span class='labello'>"+content+"</span><br><div id='modann'><button type='button' onclick='modifica_annotazione("+identificatore+")' class='btn btn-primary btn-xs modifica_ann num"+identificatore+"'><span id='modann'>modify</span></button>  <button type='button' onclick='cancella_annotazione("+identificatore+")' class='btn btn-primary btn-xs cancella_ann num"+identificatore+"'><span id='modann'>delete</span></button></div><p class='fraseee'>"+frase+"</p></div></li>");
		}
		if(json.type == "cites"){
			var title = JSON.stringify(json.label);
			var content = JSON.stringify(json.body.resource.label);

			title=rimuovi_backslash(title);
			content=rimuovi_backslash(content);
			ul.append("<li><div class='minicard'><div class='minicard-head'>"+title+"</div><div class='minicard-body'><span class='labello'>"+content+"</span><br><div id='modann'><button type='button' onclick='modifica_annotazione("+identificatore+")' class='btn btn-primary btn-xs modifica_ann num"+identificatore+"'><span id='modann'>modify</span></button>  <button type='button' onclick='cancella_annotazione("+identificatore+")' class='btn btn-primary btn-xs cancella_ann num"+identificatore+"'><span id='modann'>delete</span></button></div><p class='fraseee'>"+frase+"</p></div></li>");
		}
		/*if(json.type == "cites"){
			//console.log(JSON.stringify(json.body.resource.annotations[2][[0]])); //questo e' per ciclare sugli autori delle citazioni con j = 2
			var j = 0, k = 0;
			for (k = 0; k < json.body.resource.annotations.length; k++){
				if(json.body.resource.annotations[k].type == "hasTitle" || json.body.resource.annotations[k].type == "hasPublicationYear"){
					
					var title = JSON.stringify(json.body.resource.annotations[k].label);
					var content = JSON.stringify(JSON.stringify(json.body.resource.annotations[k].body.object));

					title=rimuovi_backslash(title);
					content=rimuovi_backslash(content);

					ul.append("<li><div class='minicard'><div class='minicard-head'>"+title+" di citazione</div><div class='minicard-body'><span class='labello'>"+content+"</span><br><div id='modann'><button type='button' onclick='modifica_annotazione("+identificatore+")' class='btn btn-primary btn-xs modifica_ann num"+identificatore+"'><span id='modann'>modify</span></button>  <button type='button' onclick='cancella_annotazione("+identificatore+")' class='btn btn-primary btn-xs cancella_ann num"+identificatore+"'><span id='modann'>delete</span></button></div><p class='fraseee'>"+frase+"</p></div></li>");
				}
			}
			for (j=0; j<json.body.resource.annotations[2].length; j++){
				var title = JSON.stringify(json.label)
				var content = JSON.stringify(JSON.stringify(json.body.resource.annotations[2][[j]].body.label));

				title=rimuovi_backslash(title);
				content=rimuovi_backslash(content);
				
				ul.append("<li><div class='minicard'><div class='minicard-head'>"+title+"</div><div class='minicard-body'><span class='labello'>"+content+"</span><br><div id='modann'><button type='button' onclick='modifica_annotazione("+identificatore+")' class='btn btn-primary btn-xs modifica_ann num"+identificatore+"'><span id='modann'>modify</span></button>  <button type='button' onclick='cancella_annotazione("+identificatore+")' class='btn btn-primary btn-xs cancella_ann num"+identificatore+"'><span id='modann'>delete</span></button></div><p class='fraseee'>"+frase+"</p></div></li>");
			}
		}*/
	
		ul.append("</ul>");
		$("div#sidr2").append(ul);
	
}

function rimuovi_backslash(stringa) {

	if(stringa==undefined) {
		return("");
	}


	while(stringa.indexOf('\\')>0) {
		stringa=stringa.replace('\\',"");
	}
	return(stringa);
}