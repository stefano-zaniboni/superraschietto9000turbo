var lista_json=[];
var lista_gruppi = [];
var ultimo=-1;
//var lista_json_gruppi=[];
var url_doc_visual;
var label_diz=[];
var indice_label=0;

var testo_intero_iniziale;
var html_intero_filtrato;
var numero_nodi;

//massimo valore attuale e' 37 
var mostra_nascondi_circoletto = 0; //questa variabile serve per determinare quando come dove e perche' il circoletto deve essere mostrato oppure no

//questa variabile server a rendere in maniera un po' brutta non cliccabili gli span in modalità reader se le annotazioni ancora non sono state caricate
var cliccabile=1;

var forzato=0;

var elenco_autori_doc_visualizzato=[];

var url_di_passaggio;

//qua dentro ci vanno a finire le nostre annotazioni scartate dalla visualizzazione perchè o fanno danni al documento o non hanno target valido
var ann_nostre_scartate=[];

$(document).ready(function() {
	//cose fighe in fade abbestia
	$("#fadeTitle").hide(0).delay(500).fadeIn(700);
	$("#fadeInstructions").hide(0).delay(1500).fadeIn(700);
	$("#fadeCentral").hide(0).delay(2000).fadeIn(700);
	$("#fadeButtons").hide(0).delay(2500).fadeIn(700);

	/*controllo per la modalita' annotator:
	* visto che all'avvio non si e' mai in annotator 
	* il pannello di Annotazione e' nascosto
	* dopo il login torna visibile */
	if(is_logged == false){
		$('#annotamelotuttobella').hide();
		$('#creatore').hide();
		$('#logout').hide();

		$("span").css("cursor","pointer");
	}


	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		$(".navbar-fixed-top").removeClass("navbar-fixed-top");
		$(".label_locali").removeProp("display","top","float");
 		console.log(navigator.userAgent);
	}


	$('#salva_dati_iniziali').click(function(){
		if(valida_dati_login()==true) {
		
			$('#lettore').hide();
			$('#creatore').show();
			$('#logout').show();

			if(is_logged==false) {
				$(".modifica_ann").hide();
				$(".cancella_ann").hide();

				$("span").css("cursor","pointer");
			}
			else {
				$(".modifica_ann").show();
				$(".cancella_ann").show();

				$("span").css("cursor","");
			}



			//nascondi_annotazioni_altri_gruppi();

		}
	});

	/* ora controllo il logout che e' sostanzialmente l'inverso,
	* di quello che facevo prima*/
	$('#sloggami').click(function(){
		is_logged = false;
		$('#lettore').show();
		$('#creatore').hide();
		$('#logout').hide();
		$('#annotamelotuttobella').hide();

		if(is_logged==false) {
			$(".modifica_ann").hide();
			$(".cancella_ann").hide();

			$("span").css("cursor","pointer");
		}
		else {
			$(".modifica_ann").show();
			$(".cancella_ann").show();

			$("span").css("cursor","");
		}

	});
	



	//al click su bottone Home tutta la pagina viene ricaricata
	$("#force-reload").click(function(){
		window.location.reload();
	});
	
	$( '#myModal' ).modal( 'toggle' );

	//se si cerca un documento
	$("form#barra").submit(function(event) {
		event.preventDefault();
		var cerca = $("#autocomplete").val();
		//console.log(cerca);

		//se c'è un https, diventa http
		if(cerca.indexOf("https://")>-1) {
			cerca=cerca.replace("https","http");
			//console.log("1",cerca);
		}
		//se non c'è un https, controllo che ci sia un http
		else if(cerca.indexOf("http://")<0) {
			cerca="http://"+cerca;
			//console.log("2",cerca);
		}
		
		//console.log(cerca);
      	//aggiunge alla lista dei documenti il documento se non è tra quelli soliti. Il funzionamento è solo locale
		controlla_se_lista(cerca);

		console.log("sto per mostrare");

		visualizza_documento_goutte(cerca);
	});

	$("#cancellatutto").click(function(){
		if(is_logged==true) {
			cancella_tutto();
		}
		else {
			alert("Non sei in modalità annotator");
		}
	});
	/*questa funzione servira' per salvare tutto sul nostro sparql ma bisogna ancora aggiustarla*/
	$("#salvatutto").click(function(){
		
		if(is_logged==true){
			salva_tutto();
		}
		else {
			alert("Non sei in modalità annotator");
		}
	});

    /* 
    * MI RICAVO GLI ID DI OGNI GRUPPO E LI METTO IN UN ARRAY
    * applico il JSON.parse(data) perche' cosi trasformo data in 
    * un oggetto json e posso accedere ai vari campi del
    * json con .[] 
    */
    $.ajax({
    	type: "GET",
    	url: "core/lib/scraper/idGruppi.php",
    	success: function(data)
    	{
    		//nel file php faccio ritornara una volta per sorta l'id gruppo e il nome
    		//salta serve per saltare appunto una cella per evitare di considerare i nomi
    		//k server per escludere il primo elemento.
    		var salta=1,k=0;
    		var nome;
    		var ul = $("<ul>");
    		//console.log(JSON.parse(data)[1]);
    		for(i in JSON.parse(data)){
    			if(salta==0) {
	    			var gr=JSON.parse(data)[i];
	    			//così facendo in lista_gruppi dovrò comunque partire a contare da 1, ma almeno id-gruppi non viene visualizzato nella pagina
	    			if(k==0) ul.append("<li hidden><button type='button' id="+gr+" class='btn btn-info btn-xs filtri' onclick='filtra_gruppi("+gr+")'>"+nome+"</button></li>");
	    			else ul.append("<li><button type='button' id="+gr+" class='btn btn-info btn-xs filtri' onclick='filtra_gruppi("+gr+")'>"+nome+"</button></li>");
	    			//console.log(JSON.parse(data)[i]);
	    			k++;
	    			//console.log(JSON.parse(data)[i+1]);
	    			lista_gruppi.push(JSON.parse(data)[i]);
	    			salta=1;
	    		}
	    		else {
	    			salta=0;
	    			nome=JSON.parse(data)[i];
	    			//console.log(nome);
	    		}
    		}
    		$("div#gruppi").append(ul);
    		var asp=setInterval(function() {
    			visualizza_documento_goutte("http://www.dlib.org/dlib/november14/brook/11brook.html");
    			clearInterval(asp);
    		},1200);

    	},
    	error: function()
    	{
    		alert("Scraper tabella gruppi fallito");
    	}
    });

    //i commenti a questa funzione valgono per tutti i pulsanti
    //ho notato che se si vogliono nascondere le annotazioni di un certo gruppo, e poi si agisce su i filtri di tipo, 
    //poi le eventuali annotazioni nascoste tornano visibili. Valutare se correggere o meno questo comportamento.
    $("#mostra1").click(function(){
    	var data=lista_json[ultimo];
		var id_bottoni=["#mostra1"];
		var tipi=["hasAuthor"];
		var classi=["autore"];
    	if($("#mostra1").hasClass("btn-danger")) {
  			//questa chiamata serve per creare le annotazioni che eventualmente abbiamo aggiunto a mano
    		//visualizza_annotazioni(data,id_bottoni,tipi,classi);
    		//visto che i pulsanti vengono premuti dopo che le annotazioni sono già state create, 
    		//per rendere visibili le annotazioni degli altri basta cambiare la loro classe e basta
    		visualizza_annotazioni_precedentemente_create("autore");
    		$(id_bottoni[0]).removeClass("btn-danger").addClass("btn-success"); 
    	}
    	else {
    		nascondi_annotazioni(id_bottoni,classi);
    	}
    });

    $("#mostra2").click(function(){
    	var data=lista_json[ultimo];
		var id_bottoni=["#mostra2"];
		var tipi=["hasPublicationYear"];
		var classi=["data"];
    	if($("#mostra2").hasClass("btn-danger")) {
    		//visualizza_annotazioni(data,id_bottoni,tipi,classi);
    		visualizza_annotazioni_precedentemente_create("data");
    		$(id_bottoni[0]).removeClass("btn-danger").addClass("btn-success");
    	}
    	else {
    		nascondi_annotazioni(id_bottoni,classi);
    	}
    });

    $("#mostra3").click(function(){
    	var data=lista_json[ultimo];
		var id_bottoni=["#mostra3"];
		var tipi=["hasDOI"];
		var classi=["DOI"];
    	if($("#mostra3").hasClass("btn-danger")) {
    		//visualizza_annotazioni(data,id_bottoni,tipi,classi);
    		visualizza_annotazioni_precedentemente_create("DOI");
    		$(id_bottoni[0]).removeClass("btn-danger").addClass("btn-success");
    	}
    	else {
    		nascondi_annotazioni(id_bottoni,classi);
    	}
    });

    $("#mostra4").click(function(){
    	var data=lista_json[ultimo];
		var id_bottoni=["#mostra4"];
		var tipi=["hasTitle"];
		var classi=["titolo"];
    	if($("#mostra4").hasClass("btn-danger")) {
    		//visualizza_annotazioni(data,id_bottoni,tipi,classi);
    		visualizza_annotazioni_precedentemente_create("titolo");
    		$(id_bottoni[0]).removeClass("btn-danger").addClass("btn-success");
    	}
    	else {
    		nascondi_annotazioni(id_bottoni,classi);
    	}
    });

    $("#mostra5").click(function(){
    	var data=lista_json[ultimo];
		var id_bottoni=["#mostra5"];
		var tipi=["hasComment"];
		var classi=["commento"];
    	if($("#mostra5").hasClass("btn-danger")) {
    		//visualizza_annotazioni(data,id_bottoni,tipi,classi);
    		visualizza_annotazioni_precedentemente_create("commento");
    		$(id_bottoni[0]).removeClass("btn-danger").addClass("btn-success");
    	}
    	else {
    		nascondi_annotazioni(id_bottoni,classi);
    	}
    });

    $("#mostra6").click(function(){
    	var data=lista_json[ultimo];
		var id_bottoni=["#mostra6"];
		var tipi=["denotesRhetoric"];
		var classi=["retorica"];
    	if($("#mostra6").hasClass("btn-danger")) {
    		//visualizza_annotazioni(data,id_bottoni,tipi,classi);
    		visualizza_annotazioni_precedentemente_create("retorica");
    		$(id_bottoni[0]).removeClass("btn-danger").addClass("btn-success");
    	}
    	else {
    		nascondi_annotazioni(id_bottoni,classi);
    	}
    });

    $("#mostra7").click(function(){
    	var data=lista_json[ultimo];
		var id_bottoni=["#mostra7"];
		var tipi=["cites"];
		var classi=["citazione"];
    	if($("#mostra7").hasClass("btn-danger")) {
    		//visualizza_annotazioni(data,id_bottoni,tipi,classi);
    		visualizza_annotazioni_precedentemente_create("citazione");
    		$(id_bottoni[0]).removeClass("btn-danger").addClass("btn-success");
    	}
    	else {
    		nascondi_annotazioni(id_bottoni,classi);
    	}
    });


    $("#mostra8").click(function(){
    	var data=lista_json[ultimo];
		var id_bottoni=["#mostra8"];
		var tipi=["hasURL"];
		var classi=["url"];
    	if($("#mostra8").hasClass("btn-danger")) {
    		//visualizza_annotazioni(data,id_bottoni,tipi,classi);
    		visualizza_annotazioni_precedentemente_create("url");
    		$(id_bottoni[0]).removeClass("btn-danger").addClass("btn-success");
    	}
    	else {
    		nascondi_annotazioni(id_bottoni,classi);
    	}
    });


    $("#mostra_TUTTI_frammenti").click(function(){
    	var data=lista_json[ultimo];
    	var id_bottoni=["#mostra1","#mostra2","#mostra3","#mostra4","#mostra5","#mostra6","#mostra7","#mostra8"];
		var tipi=["hasAuthor","hasPublicationYear","hasDOI","hasTitle","hasComment","denotesRhetoric","cites","hasURL"];
		var classi=["autore","data","DOI","titolo","commento","retorica","citazione","url"];
		if($("#mostra_TUTTI_frammenti").hasClass("btn-danger")) {
    		//visualizza_annotazioni(data,id_bottoni,tipi,classi);
    		var k;
    		for(k=0;k<classi.length;k++) {
    			visualizza_annotazioni_precedentemente_create(classi[k]);
    			$(id_bottoni[k]).removeClass("btn-danger").addClass("btn-success");
    		}
    		$("#mostra_TUTTI_frammenti").removeClass("btn-danger").addClass("btn-success");
    	}
    	else {
    		nascondi_annotazioni(id_bottoni,classi);
    		$("#mostra_TUTTI_frammenti").removeClass("btn-success").addClass("btn-danger");
    	}
    });

    
	


/*

	//Codice per la comparsa del menù di selezione per i widget sui frammenti
	var nascoto=0;
	$(function(){
		$(".nasc").hide();
	})
	$(function() {
		$(".nonasc").click(function() {
			if(nascoto==0) {
				$(".nasc").show();
				nascoto=1;
			}
			else {
				$(".nasc").hide();
				nascoto=0;
			}
		})
	})
*/

	

	//funzione di prova attivabile con un click sul pulsante data della modalità annotator
	$("#date").click(function() {
		var nuovo_json=crea_json_selezione("hasPublicationYear");
	})

	//Funzione provvisoria e da perfezionare per mostrare il pulsante per le annotazioni su tutto il documento
	$("#MostraAnnotazioni").click(function() {
		$("#mostraAnnDocumento").hide();

	})



	//Comparsa finestra modale per aggiornare informazioni utente
	$("#agg_u_m").click(function() {
		$('#modale_iniziale').modal('show');
	})

	

	//About
	$("#about").click(function() {
		$('#modal_about').modal('show');
	})

	//Contact
	$("#contact").click(function() {
		$('#modal_contact').modal('show');
	})

	//Ritorna
	/*$(".ritorna").click(function() {
		$("#edit").hide();
	})*/
	/*
	$("#showEdit").click(function() {
		$("#edit").show();
	})
	$("#showView").click(function() {
		$("#edit").hide();
	})*/

	/*$(".nasc2").hide();
	var nascosto2=0;
	$(".nonasc2").click(function() {
		if(nascosto2==0) {
			$(".nasc2").show();
			nascosto2=1;
		}
		else {
			$(".nasc2").hide();
			nascosto2=0;
		}
	})*/
	//funzioni per barre a scomparsa
	//istruzione per far comparire e scomparire la barra a lato
	 /*$('#simple-menu').sidr({
      name: 'sidr',
      side: 'left' // By default
    });*/
    $('#simple-menu2').sidr({
      name: 'sidr2',
      side: 'left'
    });
});

//istruzioni da ultimare per fare vedere chi ha fatto un'annotazione facendo click sopra di essa
//ricordarsi che le modifiche apportate alla prima parte vanno fatte anche dentro il while
$("body").on("click","span",function(e){
	if((cliccabile==0)&&(!(($(e.currentTarget)).hasClass("labello")))) {
		if (is_logged == false){
			var dettagli=[];
			var elemento=$(e.currentTarget);
			try {
				//la visualizzazione delle label viene disattivata se lo span in questione ha una classe cera
				//mettendo questa condizioen qua, anche se lo span più interno dovesse avere la classe cera, 
				//lo stop propagation non viene eseguito e quindi dovrebbe risalire e continuare ad andare
				if(elemento.attr("class")!=undefined) {
					if(elemento.attr("class").indexOf("cera")<0) {
						var gruppo=$(e.currentTarget).attr("class").substring($(e.currentTarget).attr("class").indexOf("ltw"),$(e.currentTarget).attr("class").indexOf("ltw")+7);
						var colore1=$(e.currentTarget).attr("class");
						/*console.log($(e.currentTarget));
						console.log(colore1);
						console.log($($(e.currentTarget).context));
						console.log(colore2);*/

						var col;
						if(colore1.indexOf("colore")>-1) {
							col=trova_colore(colore1,0);
						}
						else {
							col=trova_colore(colore1,1);

						}
						//console.log(col);

						var label_id=elemento.attr("id");
						var label;
						var quando;

						

						var j;
						for(j=0;j<label_diz.length;j++) {
							if(label_diz[j].key==label_id) {
								label=(label_diz[j].value);
								quando=(label_diz[j].quando);
							}
						}
						var informazioni={
							gruppo: gruppo,
							label: label,
							quando: quando,
							col: col

						};
						dettagli.push(informazioni);
						//Ricavo l'xpath
						//var prova=getXPath(this);
						//console.log(prova);
						//console.log((elemento.parent()).is("span"));
						//while(elemento.parent().hasClass(colore)) {
						while(elemento.parent().is("span")) {
							elemento=elemento.parent();
							if(elemento.attr("class")!=undefined) {
								if(elemento.attr("class").indexOf("cera")<0) {
									gruppo=elemento.attr("class").substring(elemento.attr("class").indexOf("ltw"),elemento.attr("class").indexOf("ltw")+7);

									var label_id=elemento.attr("id");
									var label;
									var quando;

									//istruzioni per trovare il colore
									var colore1=elemento.attr("class");
									

									var colBis;
									if(colore1.indexOf("colore")>-1) {
										colBis=trova_colore(colore1,0);
									}
									else {
										colBis=trova_colore(colore1,1);

									}



									
									//console.log(colBis);

									var j;
									for(j=0;j<label_diz.length;j++) {
										if(label_diz[j].key==label_id) {
											label=(label_diz[j].value);
											quando=(label_diz[j].quando);
										}
									}
									var informazioni={
										gruppo: gruppo,
										label: label,
										quando: quando,
										col: colBis
									};
									dettagli.push(informazioni);
								}
							}
						}

						//ISTRUZIONE CHIAVE
						e.stopPropagation();

						//console.log(dettagli);
						dettagli.sort(function(a, b){
							//console.log(a,b);
							if (a.gruppo < b.gruppo)
							    return -1;
							  else if (a.gruppo > b.gruppo)
							    return 1;
							  else 
							    return 0;
						});

						//codice per le label 
						$("div#labmod").html("");
						var ol = $("<ol>");
						for(j=0;j<dettagli.length;j++) { //dentro a dettagli ci stanno tutti i dettagli di cio che ho pigiato nella doc area
							var group = dettagli[j].gruppo;
							var infos = dettagli[j].label;
							var quando=dettagli[j].quando;
							var collo=dettagli[j].col;
							var backup_quando=quando;
							try {
								quando=quando.split("T");
		 						quando=quando[0]+" alle "+quando[1].substring(0,8);
		 					}
		 					catch(e) {
		 						quando=backup_quando;
		 					}
		 				
							if(collo==undefined) collo="Tipo";
							//ol.append('<li><strong>Il gruppo </strong>'+group+'<strong> labelliza </strong>'+infos+' <strong>creata il</strong> '+quando+'</li>');
							ol.append("<li><strong>L'annotazione selezionata è di tipo </strong><br>"+collo+';'+"<br><strong>L'annotazione è stata creata dal gruppo </strong><br>"+group+';'+"<br><strong>La sua label corrispondente è la seguente </strong><br>"+infos+';'+"<br><strong>L'annotazione è stata creata il </strong><br>"+quando+'.'+"<br><br></li>");
						}
						ol.append("</ol>");
						$("div#labmod").append(ol);
						$("div#labmodal").modal('show');
					}
				}
			}
			catch(r) {

			}
		}
	}
});


$("#ripristino").hide();

$("#forza_scraper").click(function(){
	//console.log(lista_json);
	lista_json.pop();
	ultimo=ultimo-1;
	//console.log(lista_json);
	$("#risultato").html(html_intero_filtrato);
	riporta_pagina_situazione_iniziale();
	numero_nodi=$(document.getElementById("risultato")).find("*");
	numero_nodi=numero_nodi.length;
	forzato=1;
	scraper_automatico(url_doc_visual);
	$("#ripristino").show();
});

//quando si modifica lancia query bisogna modificare anche qua
$("#ripristino").click(function(){
	for(i=1;i<lista_gruppi.length;i++) {
		lista_gruppi[i]=lista_gruppi[i].trim();
		//console.log(lista_gruppi[i]);
		if(lista_gruppi[i]!="ltw1511") {
			//per ora scarto i gruppi sopra elencati
			if((lista_gruppi[i]!="ltw1516")&&(lista_gruppi[i]!="ltw1520")&&(lista_gruppi[i]!="ltw1542")&&(lista_gruppi[i]!="ltw1544")) {
				//if(lista_gruppi[i]=="ltw1508"){
					var url="http://vitali.web.cs.unibo.it/raschietto/graph/"+lista_gruppi[i];
					//console.log(url_doc_visual,url);
					lancia_query(url_doc_visual,url,0);  //disattivo le annotazioni degli altri
				//}
			}
		}
	}
	$("#ripristino").hide();
});

$("#aiuto").click(function(){
	$("#modale_aiuto").modal("show");
});

$("#chiudi_info").click(function() {
	$("#modale_aiuto").modal("toggle");
});



$("#annulla").click(function(){
   	$("#modale_cambiamento_documento").modal("toggle");
});

$("#continua_senza_salvare").click(function(){
	$("#modale_cambiamento_documento").modal("toggle");
		visualizza_documento_goutte(url_di_passaggio);
});	

$("#salva_e_continua").click(function(){
	$("#modale_cambiamento_documento").modal("toggle");

	salva_tutto();

	visualizza_documento_goutte(url_di_passaggio);
});





$(".scartate").dblclick(function() {
	try{
		var p=$("<p>");
		var cic;
		for(cic=0;cic<ann_nostre_scartate.length;cic++) {
			
			var j=JSON.stringify(ann_nostre_scartate[cic].type);
			p.append("<p>"+j+"</p>");
			p.append("<br>");
		}
		p.append("</p>");
		$("#scartate_body").append(p);
		$("#modale_scartate").modal("show");
	}
	catch(e) {

	}
});

$("#chiudi_scartate").click(function(){
	$("#modale_scartate").modal("toggle");
	$("#scartate_body").html("");
});

function riporta_pagina_situazione_iniziale() {
	$("#mostra1").removeClass("btn-success").addClass("btn-danger");
	$("#mostra2").removeClass("btn-success").addClass("btn-danger");
	$("#mostra3").removeClass("btn-success").addClass("btn-danger");
	$("#mostra4").removeClass("btn-success").addClass("btn-danger");
	$("#mostra5").removeClass("btn-success").addClass("btn-danger");
	$("#mostra6").removeClass("btn-success").addClass("btn-danger");
	$("#mostra7").removeClass("btn-success").addClass("btn-danger");
	$("#mostra8").removeClass("btn-success").addClass("btn-danger");
	$("#mostra_TUTTI_frammenti").removeClass("btn-success").addClass("btn-danger");

	$("#barra_di_progresso").attr("style","width:0%");
	$("#barra_di_progresso").addClass("active");

	$(".filtri").removeClass("btn-primary").addClass("btn-info");
	$(".filtri").removeClass("btn-default").addClass("btn-info");


	$("#sidr2").empty();

	$(".container_progresso").show();
	mostra_nascondi_circoletto=0;

	forzato=0;

	elenco_autori_doc_visualizzato=[];

	ann_nostre_scartate=[];

}

function visualizza_documento_goutte(cerca) {

	//istruzione per ripotare la pagina alla situazione iniziale
	riporta_pagina_situazione_iniziale();

	$.ajax({
            //definisco il tipo della chiamata
            type: "GET",
            //url della risorsa da contattare
            url: "core/proxy.php?url="+cerca,
            //azione in caso di successo
            success: function(data)
            {
            	url_doc_visual=cerca;
            	if(url_doc_visual.indexOf(".html")<0) url_doc_visual=url_doc_visual+".html";

            	//controllo forse non sempre azzeccato per eliminare la stringa accept cookies dall'url
            	if(url_doc_visual.indexOf("?")>0) {
            		var k;
            		for(k=url_doc_visual.indexOf("?");(k<url_doc_visual.length)&&(url_doc_visual.charAt(k)!=".");k++) {

            		}
            		var sinistra=url_doc_visual.substring(0,url_doc_visual.indexOf("?"));
            		var destra=url_doc_visual.substring(k,url_doc_visual.length);
            		//console.log(sinistra,destra);
            		url_doc_visual=sinistra+destra;
            	}

            	//ROBERT 
				//data=data.replace(/^\s*/, '').replace(/\s*$/, '');
				//data=data.trim();

            	var contenuto=$.parseHTML(data);
				$(contenuto).find("script").remove();
				$(contenuto).find("style").remove();
				$(contenuto).find("link").remove();
				//$(contenuto).find("img").remove();
				//$(contenuto).find("br").remove();
				//non penso funzioni
				$(contenuto).removeClass();
				contenuto=risolvi_immagine(contenuto);

				//fantastiche righe di grasso
				$(contenuto).find('a').each(function(){
        			var cnt=$(this).contents();
					$(this).replaceWith(cnt);
				});

				$(contenuto).find('b').each(function(){
        			var cnt=$(this).contents();
					$(this).replaceWith(cnt);
				});

				$(contenuto).find('i').each(function(){
        			var cnt=$(this).contents();
					$(this).replaceWith(cnt);
				});



				//console.log(contenuto);
				
				//document.write(contenuto);


				$("#risultato").html(contenuto);
				//document.write($("div#risultato").html());

				testo_intero_iniziale=$(document.getElementById("risultato")).text();
				html_intero_filtrato=$(document.getElementById("risultato")).html();
				
				numero_nodi=$(document.getElementById("risultato")).find("*");
				numero_nodi=numero_nodi.length;
				//console.log(numero_nodi);

				//genero il json degli altri gruppi e visualizzo le loro annotazioni
				cicla_sui_gruppi_e_visualizza_annotazioni();

			},
            //azione in caso di errore
            error: function()
            {
            	alert("Chiamata proxy fallita, documento non caricato");
            }
        });
}

function scraper_automatico(cerca) {
	//console.log(cerca);
	//console.log(url_doc_visual);
	$.ajax({
    		//definisco il tipo della chiamata
    		type: "GET",
    		//url della risorsa da contattare
    		url: "core/scrape.php?url="+cerca,
    		dataType: 'json',
    		//azione in caso di successo
    		success: function(data)
    		{

    			var fix_n;
    			for(fix_n=0;fix_n<data.annotations.length;fix_n++) {
    				data.annotations[fix_n]=parsa_i_numeri(data.annotations[fix_n]);
    			}

    			lista_json.push(data);
		   		ultimo=ultimo+1;		//dentro a lista_json[ultimo] ci sarà l'oggetto json del documento attualmente visualizzato
		   		
		   		//salvata = 0 vuol dire che è stata messa sullo sparql

		   		
		   		var json_steve={
							annotations: [],
							"provenance": {
					        	"author": {
					            	"name": "",
					            	"email": ""
					        	},
					        	"time": ""
					    	}
						};

		   		var k;
		   		for(k=0;k<lista_json[ultimo].annotations.length;k++) {
		   			lista_json[ultimo].annotations[k].salvata=0;
		   			if(lista_json[ultimo].annotations[k].type=="cites") {
		   				var l;
		   				for(l=0;l<lista_json[ultimo].annotations[k].body.resource.annotations.length;l++) {
		   					//citazione_per_stefano.push(lista_json[ultimo].annotations[k].body.resource.annotations[l]);
		   					json_steve.annotations.push(lista_json[ultimo].annotations[k].body.resource.annotations[l]);
		   				}
		   				//citazione_per_stefano.push(lista_json[ultimo].annotations[k].body.resource);
		   				//json_steve.annotations.push(citazione_per_stefano);
		   				json_steve.provenance.author.name = lista_json[ultimo].provenance.author.name;
		   				json_steve.provenance.author.email = lista_json[ultimo].provenance.author.email;
		   				json_steve.provenance.time = lista_json[ultimo].provenance.time;
		   			}
		   		}
		   		if(json_steve.annotations.length>0) {
		   			var g;
		   			for(g=0;g<json_steve.annotations.length;g++) {
		   				json_steve.annotations[g].salvata=0;
		   			}

		   			//console.log("Sottocampi citazioni ", JSON.stringify(json_steve));
		   		}
		   		else console.log("Sottocampi per citazione non presenti");

		   		//document.write(JSON.stringify(data));
		   		console.log(JSON.stringify(data)); //json del nostro scraper, scommentare per vederlo in console
				console.log("Il nostro scraper ha concluso");
				console.log("sto per partire");
				conversione_turtle(data); //conversione immediata in turtle e invio allo sparql
				conversione_turtle(json_steve);
				var id_bottoni=["#mostra1","#mostra2","#mostra3","#mostra4","#mostra5","#mostra6","#mostra7","#mostra8"];
				var tipi=["hasAuthor","hasPublicationYear","hasDOI","hasTitle","hasComment","denotesRhetoric","cites","hasURL"];
				var classi=["autore","data","DOI","titolo","commento","retorica","citazione","url"];
				visualizza_annotazioni(data,id_bottoni,tipi,classi,"ltw1511");
				visualizza_annotazioni(json_steve,id_bottoni,tipi,classi,"ltw1511");
				$("#mostra_TUTTI_frammenti").removeClass("btn-danger").addClass("btn-success");
				//console.log("Le nostre annotazioni sono state caricate");

				mostra_nascondi_circoletto = mostra_nascondi_circoletto + 1;
				//console.log(mostra_nascondi_circoletto);
				if(forzato==0) becker(mostra_nascondi_circoletto);
				else {
					becker(38);
				}
			},
        	//azione in caso di errore
        	error: function()
        	{
        		becker(38);
        		alert("Chiamata Scraper fallita, json non creato");
        	}
        });
}

//nuova va messo a 1 se si sta facendo un'annotazione nuova
//così facendo, se si fa una annotazione dello stesso tipo sopra quella di un altro gruppo
//non accadono casini
function visualizza_annotazioni(json,id_pulsante,tipo,classe,gruppo,nuova) {
	if(nuova===undefined) nuova=0;
	var i;
	//console.log(JSON.stringify(json),id_pulsante,tipo,classe);
	if(json===undefined) return(0);
	if(json.annotations.length==0) {
		if(!(gruppo===undefined)) {
			$("#"+gruppo).removeClass("btn-info").addClass("btn-default");
		}
	}
	//console.log(json.annotations.length);
	for(i=0;i<json.annotations.length;i++) {
		if(json.annotations[i]!="invalid") {
			console.log(json.annotations[i].type);
		}
		else console.log("Invalid");

		if((json.annotations[i].type=="hasUrl")&&(gruppo=="ltw1511")) {
			json.annotations[i].type="hasURL";
		}

		var valida,selettore;
		valida=controllo_annotazione(json.annotations[i]);
		if(valida==1) {
			selettore=json.annotations[i].target.id;
			//console.log(selettore);
			selettore=funzione_che_trasforma_in_xpath(selettore,gruppo);
			//console.log(selettore);
			//console.log(json.annotations[i].type);
			if(selettore!=0) {
				var selezione=$(document.getElementById("risultato")).xpath(selettore).html();
				
				var start=json.annotations[i].target.start;
				var end=json.annotations[i].target.end;
				var json_type=json.annotations[i].type;

				/*var debug=selezione.split("");
				var prova;
				if(json_type=="cites") {
					for(prova=0;prova<debug.length;prova++) {
						console.log(String(debug[prova]).charCodeAt(0),debug[prova],"Indice",prova);
					}
					console.log(debug);
				}*/
				if(json_type.indexOf("fabio:")>-1) {
					json_type=json_type.replace("fabio:","");
				}
				if(json_type.indexOf("fabio")>-1) {
					json_type=json_type.replace("fabio","");
				}
				if(json_type.indexOf("dcterms:")>-1) {
					json_type=json_type.replace("dcterms:","");
				}

				var k;
				//console.log(selezione,start,end,json_type);
				for(k=0;k<tipo.length;k++) {
					if(tipo[k]==json_type) {
						if(($(document.getElementById("risultato")).xpath(selettore+"//span").hasClass("cera_"+classe[k]))&&(nuova==0)) {
							console.log("L'annotazione era già stata creata, aggiungo solo la classe");
							//$(document.getElementById("risultato")).xpath(selettore+"//span[@class=\"cera_"+classe[k]+"\"]").removeClass("cera_"+classe[k]).addClass("colore_"+classe[k]);
							visualizza_annotazioni_precedentemente_create(classe[k]);
						}
						else {
							
							var start_mod=parseInt(start)+conta_tag(parseInt(start),selezione,0);
							//console.log("start_mod:",start_mod);
							var end_mod=parseInt(end)+conta_tag(parseInt(end),selezione,1);
							//console.log("end_mod:",end_mod);
							//per ora ho inizializzato gruppo così, però il valore che prende non è quello giusto,
							//se il gruppo non è stato calcolato correttamente non aggiungerlo come classe
							/*console.log("dettagli: ");
							console.log(start,end);
							console.log(start_mod,end_mod);
							console.log(selettore);
							console.log("prima:");
							console.log(selezione);*/
							if(gruppo===undefined) selezione=aggiungi_span_con_classe_adeguata(selezione,start_mod,end_mod,"colore_"+classe[k]);
							else selezione=aggiungi_span_con_classe_adeguata(selezione,start_mod,end_mod,"colore_"+classe[k],gruppo);
							/*console.log("dopo:");
							console.log(selezione);*/

							
							//istruzioni nuovo per gestire gli errori del documento
							var copia_intero_documento=$(document.getElementById("risultato")).clone();
							copia_intero_documento.xpath(selettore).html(selezione);

							/*var test_nodi=copia_intero_documento.find("*");
							test_nodi=test_nodi.length;*/
							
							//console.log(copia_intero_documento.text());
							//console.log(test_nodi.length);
							/*if(testo_intero_iniziale!=copia_intero_documento.text()) {
								console.log("L'annotazione modifica il testo dovrebbe essere scartata");
								if(test_nodi==(numero_nodi+1)) {
									console.log("L'annotazione verrà mostrata");
								}
								else console.log("L'annotazione verrà nascosta");
							}*/

							//if(test_nodi==(numero_nodi+1)) {
								/*if(nuova==1) {
									var k;
									for(k=0;k<100;k++) {
										if((testo_intero_iniziale.charCodeAt(k))!=(copia_intero_documento.text().charCodeAt(k))) {
											console.log(testo_intero_iniziale.substring(k-10,k+10));
											console.log(copia_intero_documento.text().substring(k-1,k+10));
										}
									}
									
								}*/
							if(testo_intero_iniziale==copia_intero_documento.text()) {
								$(document.getElementById("risultato")).xpath(selettore).html(selezione);
								if(gruppo=="ltw1511") {
									//alert("hey");
									//chiamando questa funzione oltre a visualizzare le label, associo
									//ad i bottoni lo span corrispondente

									if(json.annotations[i].salvata==0) {
										visualizza_nostre_label(json.annotations[i],indice_label,0);
									}
									else visualizza_nostre_label(json.annotations[i],indice_label,1);
									//chiamando questa funziona vado a modificare il nostro json
									//presente in lista_json[ultimo] aggiungendo un campo identificatore
									//che collega la label all'effettivo json corrispondente
									associa_label_a_json(json.annotations[i],"label"+indice_label);
								}
								if(json_type=="hasAuthor") {
									//console.log(json_type)
									//console.log(json);
									gestisci_autori(json.annotations[i]);
								}
								numero_nodi=numero_nodi+1;
							}
							else {
								if(gruppo=="ltw1511") {
									ann_nostre_scartate.push(json.annotations[i]);
								}
								console.log("Scarto l'annotazione perchè fa danni al documento");
								if(nuova==1) {
									alert("I parametri dell'annotazione non sono corretti");
								}
							}
							
							//istruzioni vecchie
							//per vedere se ho fatto dei danni per ora procedo così, anche se non mi piace:
							//modifico il testo del documento, perchè altrimenti non riuscivo ad ottenere 
							//il testo correttamente; faccio un controllo ed eventualmente ripristino il testo
							//precedente
							/*var testo_selezione_prima=$(document.getElementById("risultato")).xpath(selettore).text();
							var html_selezione_prima=$(document.getElementById("risultato")).xpath(selettore).html();
							$(document.getElementById("risultato")).xpath(selettore).html(selezione);
							var testo_selezione_dopo=$(document.getElementById("risultato")).xpath(selettore).text();
							if(!(testo_selezione_prima==testo_selezione_dopo)) {
								$(document.getElementById("risultato")).xpath(selettore).html(html_selezione_prima);
								console.log("Scarto l'annotazione perchè fa danni al documento");
							}
							else {
								testo_intero_dopo=$(document.getElementById("risultato")).text();
								if(!(testo_intero_iniziale==testo_intero_dopo)) {
									//istruzioni per il ripristino dell' html
									console.log("Dovrei scartare l'annotazione perchè fa gravi danni al documento");
								}
							}*/
							
							
							label_diz.push({ //qua metto dentro a label_diz la chiave e compongo il dizionario con le label di tutti
								key: "label"+indice_label,
								value: json.annotations[i].label,
								quando: json.provenance.time
							});
							
							indice_label++;
						}
						$(id_pulsante[k]).removeClass("btn-danger").addClass("btn-success"); 
					}
				}
			}
		}
		else {
			//istruzioni uguali a sopra per aggiungere la minicar in caso di annotazione sul documento
			if(gruppo=="ltw1511") {

				if(json.annotations[i].salvata==0) {
					visualizza_nostre_label(json.annotations[i],indice_label,0);
				}
				else visualizza_nostre_label(json.annotations[i],indice_label,1);
				associa_label_a_json(json.annotations[i],"label"+indice_label);
				label_diz.push({ 
								key: "label"+indice_label,
								value: json.annotations[i].label,
								quando: json.provenance.time
				});
							
				indice_label++;

				console.log("Annotazione nostra da mettere solo nella minicard");
				ann_nostre_scartate.push(json.annotations[i]);
			}
		}
	}
}

function nascondi_annotazioni(id_pulsante,classe) {
	var k;
	for(k=0;k<classe.length;k++) { //span[@class="colore_autore"]
		$(document.getElementById("risultato")).find($(".colore_"+classe[k])).removeClass("colore_"+classe[k]).addClass("cera_"+classe[k]);
		//$(document.getElementById("risultato")).xpath("//span[@class=\"colore_"+classe[k]+"\"]").removeClass("colore_"+classe[k]).addClass("cera_"+classe[k]);
		$(id_pulsante[k]).removeClass("btn-success").addClass("btn-danger");
		//document.write($("#risultato").html());
	}
	//console.log($(document.getElementById("risultato")).find("span"));

}

function funzione_che_trasforma_in_xpath(sel,gr) {
	if((sel=="/html/body")||(sel=="html/body")) {
		console.log("Selettore fatto dannatamente male, è solo /html/body o html/body: potrebbe fare GRAVISSIMI danni, quindi nel dubbio scarto");
		return(0);
	}
	if(sel.charAt(0)=="_") {
		console.log("Il selettore inizia con _, lo scarto");
		return(0);
	}
	var stringa=String(sel);
	var i;
	//console.log(stringa);
	var provenienza=url_doc_visual;
	while(stringa.indexOf("_")>-1) stringa=stringa.replace("_","/");
	while(stringa.indexOf("[")>-1) stringa=stringa.replace("[","");
	while(stringa.indexOf("]")>-1) stringa=stringa.replace("]","");
	while(stringa.indexOf('/tbody1')>-1) stringa=stringa.replace('/tbody1','');
	while(stringa.indexOf('/tbody')>-1) stringa=stringa.replace('/tbody','');
	if(stringa.indexOf("/html/body/")>-1) stringa=stringa.replace('/html/body/','');
	if(stringa.indexOf("html/body/")>-1) stringa=stringa.replace('html/body/','');
	//console.log(stringa);

	try{
		if(stringa.indexOf("@")>-1) {
			var s=stringa.substring(0,stringa.indexOf("@"));
			var ggg,t=0,ind=stringa.indexOf("@");
			for(ggg=stringa.indexOf("@");(t<=1)&&(ggg<stringa.length);ggg++) {
				if(stringa.charAt(ggg)=="'") t++;
			}
			if(t==0) {
				for(ggg=stringa.indexOf("@");(t<=1)&&(ggg<stringa.length);ggg++) {
					if(stringa.charAt(ggg)=="\"") t++;
				}
			}
			ind=ggg-ind;
			var w=stringa.substring(stringa.indexOf("@"),ind+3);
			var d=stringa.substring(ind+3,stringa.length);
			stringa=s+"["+w+"]"+d;
		}
	}
	catch(e) {
		console.log("Errore nell'aggiustare cambio di vincenzo");
	}


	if(stringa.indexOf("#")>-1) {
		var id_len;
		for(id_len=stringa.indexOf("#");(stringa.charAt(id_len)!="/")&&(id_len<stringa.length);id_len++) {

		}
		stringa=stringa.replace(stringa.substring(stringa.indexOf("#"),id_len),"\/\/*[@id=\""+stringa.substring(stringa.indexOf("#")+1,id_len)+"\"]");
	}

	//h3->h3 //h32-> h3[2] //div3->div[3] //div34->div[34] 
	for(i=0;i<stringa.length;i++) {
		//se incontr un h
		if(stringa.charAt(i)=="h") {
			//se sono in una situazione del genere h32
			if(numero(stringa.charAt(i+2))==1) {
				var j,k=0;
				//conto quanti numeri devo wrappare con le []
				for(j=i+2;(numero(stringa.charAt(j))==1)&&(j<stringa.length);j++) {
					//console.log(stringa.charAt(j));
					k++;
				}
				var sinistra=stringa.substring(0,i+2);
				var wrap=stringa.substring(i+2,j);
				wrap="["+wrap+"]";
				var destra=stringa.substring(j,stringa.length);
				stringa=sinistra+wrap+destra;
				i=i+2+k+1;
				
			} 
			//se c'è un semplice h3 sposto avanti i di 1
			else i=i+1;
		}
		else {
			//se incontro un numero, vuol dire che non c'è un h altrimenti la situazione sarebbe già stata gestita
			if(numero(stringa.charAt(i))==1) {
				var l,m=0;
				//conto quanti numeri devo wrappare con le []
				for(l=i;(numero(stringa.charAt(l))==1)&&(l<stringa.length);l++) {
					m++;
				}
				var sinistra=stringa.substring(0,i);
				var wrap=stringa.substring(i,l);
				wrap="["+wrap+"]";
				var destra=stringa.substring(l,stringa.length);
				stringa=sinistra+wrap+destra;
				i=i+2+m-1;
			}
		}
	}
	//alert($(document.getElementById("risultato")).xpath(stringa).html());
	//alert($(document.getElementById("risultato")).html());

	//console.log("Prima",stringa);





	//qui sto inserendo degli aggiustamenti generali 
	if(provenienza.indexOf("dlib")>-1) {
		stringa=stringa.replace("form/table[3]/tr/td/table[5]/tr/td/table/tr/td[2]/","");
		stringa=stringa.replace("form/table[3]/tr/td/table[5]/tr/td/table[1]/tr/td[2]/","");
		stringa=stringa.replace("form[1]/table[3]/tr[1]/td[1]/table[5]/tr[1]/td[1]/table[1]/tr[1]/td[2]/","");
		stringa=stringa.replace("form/table[3]/tr/td/table[5]/tr/td/table[1]/tr[1]/td[2]/","");
		stringa=stringa.replace("form/table[3]/tr/td/table[5]/tr[1]/td[1]/table[1]/tr[1]/td[2]/","");
		stringa=stringa.replace("form[1]/table[3]/tr/td/table[5]/tr/td/table[1]/tr/td[2]/","");
	}
	else if(provenienza.indexOf("unibo")>-1) {
		//console.log(stringa);
		stringa=stringa.replace("div/div2/div2/div3/","");
		stringa=stringa.replace("div/div[2]/div[2]/div[3]/","");
		stringa=stringa.replace("div[1]/div[2]/div[2]/div[3]/","");
	}
	//console.log("Dopo",stringa);
	//alert($(document.getElementById("risultato")).xpath(stringa).html());
	//alert($(document.body).xpath(stringa).html());
	//console.log($(document.getElementById("risultato")).xpath(stringa).html());
	try{
		var testa=$(document.getElementById("risultato")).xpath(stringa);
	}
	catch(err) {
		console.log("Selettore non valido");
		return(0);
	}
	//console.log(stringa);
	if($(document.getElementById("risultato")).xpath(stringa).html()===undefined) {
		if(gr=="ltw1511") {
			try {
				stringa=aggiusta_xpath1(stringa);
				if($(document.getElementById("risultato")).xpath(stringa).html()===undefined) {
					console.log("Selettore non valido dopo aggiustamento");
					return(0);
				}
				else return(stringa);
			}
			catch(e) {
				console.log("Errore aggiusta xpath1");
				return(0);
			}
		}
		console.log("Selettore non valido");
		return(0);
	}
	//console.log("Giusto xpath= ",stringa);
	return(stringa);
} 

function numero(str) {
	if((str>String.fromCharCode(47))&&(str<String.fromCharCode(58))) return(1);
	else return(0);
}

function aggiungi_span_con_classe_adeguata(stringa,start,end,classe,gruppo) {

	

	var sinistra,wrap,destra;
	sinistra=stringa.substring(0,start);
	wrap=stringa.substring(start,end);
	destra=stringa.substring(end,stringa.length);
	//console.log(start,end);
	//console.log("a",sinistra,"b",wrap,"c",destra,"d");
	if(gruppo===undefined)  stringa=sinistra+"<span id=\"label"+indice_label+"\" class=\""+classe+"\">"+wrap+"</span>"+destra; //aggiungo proprio l'indice della label
	else stringa=sinistra+"<span id=\"label"+indice_label+"\" class=\""+classe+" "+gruppo+"\">"+wrap+"</span>"+destra;
	//stringa=stringa.replace(/&nbsp;/ig,'');
	return(stringa);

	//return(stringa.replace(stringa.substring(start,end),"<span class=\""+classe+"\">"+stringa.substring(start,end)+"</span>"));
}


//queste due funzioni contano tutti i tag che incontrano, compresi i br

function conta_tag(fino_a_dove_conto,selezione,what) {
	var variazione=0,conteggio_lf=0;
	var stringa=selezione;
	//console.log("inizio",selezione,selezione.length);
	//console.log("fino",fino_a_dove_conto);
	//start
	if(what==0) {
		while((stringa.indexOf("<")>=0)&&((stringa.indexOf("<")<=fino_a_dove_conto))) {
				//console.log(stringa.indexOf("<"));
				//console.log(stringa);
				var tag = /<+?.*?>/;
				stringa=stringa.replace(tag,"");
				stringa=stringa.replace("&amp;","&");
		}
	}
	//end
	else {
		while((stringa.indexOf("<")>=0)&&((stringa.indexOf("<")<fino_a_dove_conto))) {
			//console.log(stringa.indexOf("<"));
			//console.log(stringa);
			var tag = /<+?.*?>/;
			stringa=stringa.replace(tag,"");
			stringa=stringa.replace("&amp;","&");
		}
	}
	//azioni per contare line feed
	/*var splitter=stringa.split("");
	var k;
	for(k=0;(k<=fino_a_dove_conto)&&(k<splitter.length);k++) {
		if(String(splitter[k]).charCodeAt(0)==10) {
			conteggio_lf++;
		}
	}*/
	//console.log("dopo",stringa,stringa.length);
	variazione=((selezione.length-stringa.length)+conteggio_lf);
	//console.log("variazione:",variazione);

	return(variazione);
}





function controllo_annotazione(json) {
	try {
		if(json=="invalid") {
			return(0);
		}

		if((json.target.id=="")||(typeof json.target.id==="undefined")) {
			console.log("ID non definito");
			return(0);
		}
		else if((json.target.start<0)||(json.target.end==0)||(json.target.start>=json.target.end)) {
			console.log("Start o end non validi");
			/*console.log(json);
			console.log(json.target.start);
			console.log(json.target.end);
			console.log(json.target.start<0);
			console.log(json.target.end==0);
			console.log(json.target.start>json.target.end);*/
			return(0);
		}
		if((json.type!="fabio:hasPublicationYear")&&(json.type!="fabiohasPublicationYear")&&(json.type!="hasURL")&&(json.type!="hasAuthor")&&(json.type!="hasPublicationYear")&&(json.type!="hasDOI")&&(json.type!="hasTitle")&&(json.type!="hasComment")&&(json.type!="denotesRhetoric")&&(json.type!="cites")) {
			console.log("Tipo dell'annotazione non valido");
			return(0);
		}
		return(1);
	}
	catch(e) {
		console.log("Errore in controllo");
		return(0);
	}
}

function visualizza_annotazioni_precedentemente_create(classe) {
	$(document.getElementById("risultato")).find($(".cera_"+classe)).removeClass("cera_"+classe).addClass("colore_"+classe);
}

function cicla_sui_gruppi_e_visualizza_annotazioni() {
	//ltw1536: aggiustato
	//ltw1539: aggiustato, ma hanno annotazioni poco sensate 
	//ltw1538: aggiustato, ma alcuni selettori hanno b e mi puzzano
	//ltw1542: continuo ad escluderli perchè jquery xpath si impalla e poi anche se visualizza hanno annotazioni a culo
	//ltw1545: ora vengono accettati, ma non si visualizza niente e mi fa strano
	//ltw1547: aggiustato
	//ltw1516: continuo ad escluderli, questi hanno un sacco di selettori con la a che da noi è stata cancellata, in più per qualche ragione jquery xpath si incavola 3 volte
	//ltw1512: ammessi, avevano alcune annotazioni che iniziavano con _
	//ltw1510: aggiustato
	//ltw1520: continuo ad escluderti perchè hanno annotazioni pessime
	//ltw1525: aggiustato, rilevo selettori fatti come /html/body
	//ltw1544: esclusi perchè hanno citazioni strane
	console.log("Ciclerò sui gruppi");
	//div dovrà essere uguale al numero di gruppi su cui ciclo
	var i;
	//modificare il valore in i<lista_gruppi.length
	for(i=1;i<lista_gruppi.length;i++) {
		lista_gruppi[i]=lista_gruppi[i].trim();
		//console.log(lista_gruppi[i]);
		if(lista_gruppi[i]!="ltw1511") {
			//per ora scarto i gruppi sopra elencati
			if((lista_gruppi[i]!="ltw1516")&&(lista_gruppi[i]!="ltw1520")&&(lista_gruppi[i]!="ltw1542")&&(lista_gruppi[i]!="ltw1544")) {
				//ricordarsi di modificare anche ripristino
				//if(lista_gruppi[i]=="ltw1508"){
					var url="http://vitali.web.cs.unibo.it/raschietto/graph/"+lista_gruppi[i];
					//console.log(url_doc_visual,url);
					lancia_query(url_doc_visual,url,0);  //disattivo le annotazioni degli altri
				//}
			}
			//serve a spegnere il bottone dei gruppi che escludiamo
			else {
				$("#"+lista_gruppi[i]).removeClass("btn-info").addClass("btn-default");
			}
		}
		else {
			var url="http://vitali.web.cs.unibo.it/raschietto/graph/"+lista_gruppi[i];
			lancia_query(url_doc_visual,url,1);  /* ========================================== scommentare questa riga ripristina il nostro scraper e molte cose funzionano e non si sa come */
		}

		/*$("#barra_di_progresso").attr("style","width:"+prog+"%");
		prog=prog+div;*/
	}
	//$("#barra_di_progresso").attr("style","width:100%");
	//$("#barra_di_progresso").removeClass("active");
}

//se nostro è settato a 1, se lo sparql esaminato è vuoto parte il nostro scraper
//non so cosa accade se trova qualcosa, ad esempio cosa accade a lista_json[ultimo]
function lancia_query(document_url,esempio_endpoint_gruppo,nostro) {
	//console.log(document_url);
	//if(document_url.indexOf("unibo")>=0) document_url=document_url.replace(".html","");
	//console.log(document_url);
	//var document_url="http://www.dlib.org/dlib/september14/latif/09latif.html";
	var endpoint="http://tweb2015.cs.unibo.it:8080/data/query";
	//var esempio_endpoint_gruppo = "http://vitali.web.cs.unibo.it/raschietto/graph/ltw1508";
	var query1="	PREFIX schema: <http://schema.org/>\
			PREFIX fabio: <http://purl.org/spar/fabio/>\
			PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#>\
			PREFIX skos:  <http://www.w3.org/2009/08/skos-reference/skos.html>\
			PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>\
			PREFIX frbr:  <http://purl.org/vocab/frbr/core#>\
			PREFIX cito:  <http://purl.org/spar/cito/>\
			PREFIX deo:   <http://purl.org/spar/deo/>\
			PREFIX sro:   <http://salt.semanticauthoring.org/ontologies/sro#>\
			PREFIX oa:    <http://www.w3.org/ns/oa#>\
			PREFIX rsch:  <http://vitali.web.cs.unibo.it/raschietto/>\
			PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\
			PREFIX dcterms: <http://purl.org/dc/terms/>\
			PREFIX sem:   <http://www.ontologydesignpatterns.org/cp/owl/semiotics.owl#>\
			PREFIX foaf:  <http://xmlns.com/foaf/0.1/>\
			PREFIX prism: <http://prismstandard.org/namespaces/basic/2.0/>\
			SELECT ?prd ?xpath ?start ?end ?label ?when ?who FROM <"+esempio_endpoint_gruppo+"> WHERE{\
	  			?nota a oa:Annotation;\
	   				oa:hasTarget ?trgt;\
	   				oa:annotatedAt ?when;\
					oa:annotatedBy ?who;\
					oa:hasBody ?body.\
						?body rdf:predicate ?prd.\
						OPTIONAL{\
							?body rdfs:label ?label.\
						}\
						?trgt oa:hasSource <"+document_url+">.\
						?trgt oa:hasSelector ?sel.\
  							?sel rdf:value ?xpath.\
  							OPTIONAL{\
								?sel oa:start ?start.\
								?sel oa:end ?end.\
							}\
	}";
	var query="	PREFIX schema: <http://schema.org/>\
			PREFIX fabio: <http://purl.org/spar/fabio/>\
			PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#>\
			PREFIX skos:  <http://www.w3.org/2009/08/skos-reference/skos.html>\
			PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>\
			PREFIX frbr:  <http://purl.org/vocab/frbr/core#>\
			PREFIX cito:  <http://purl.org/spar/cito/>\
			PREFIX deo:   <http://purl.org/spar/deo/>\
			PREFIX sro:   <http://salt.semanticauthoring.org/ontologies/sro#>\
			PREFIX oa:    <http://www.w3.org/ns/oa#>\
			PREFIX rsch:  <http://vitali.web.cs.unibo.it/raschietto/>\
			PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\
			PREFIX dcterms: <http://purl.org/dc/terms/>\
			PREFIX sem:   <http://www.ontologydesignpatterns.org/cp/owl/semiotics.owl#>\
			PREFIX foaf:  <http://xmlns.com/foaf/0.1/>\
			PREFIX prism: <http://prismstandard.org/namespaces/basic/2.0/>\
			SELECT ?subj ?prd ?obj ?xpath ?start ?end ?label ?when ?who FROM <"+esempio_endpoint_gruppo+"> WHERE{\
  				?nota a oa:Annotation;\
   					oa:hasTarget ?trgt;\
   					oa:annotatedAt ?when;\
					oa:annotatedBy ?who;\
					oa:hasBody ?body.\
						?body rdf:subject ?subj.\
						?body rdf:predicate ?prd.\
						?body rdf:object ?obj .\
						OPTIONAL{\
							?body rdfs:label ?label.\
						}\
							?trgt oa:hasSource <"+document_url+">.\
						?trgt oa:hasSelector ?sel.\
								?sel rdf:value ?xpath.\
							OPTIONAL{\
							?sel oa:start ?start.\
							?sel oa:end ?end.\
						}\
			}";


	
	var encode=encodeURIComponent(query);
	var queryURL=endpoint+"?query="+encode+"&format=json";
	//se sto esaminando lo sparql degli altri
	if(nostro==0) {
		$.ajax({
			dataType: "jsonp",
			url:queryURL,
			type: "GET",
			//async: "false",
			success: function(data) {
				console.log("Quste sono le annotazioni del gruppo",esempio_endpoint_gruppo);

				//console.log("La query restituisce",JSON.stringify(data.results.bindings));
				//document.write(JSON.stringify(data.results.bindings));
				data=converti_in_nostro_json(data.results.bindings);
				//document.write(JSON.stringify(data));	
				//lista_json_gruppi.push(data);
				//console.log("Json convertito=",JSON.stringify(data));

				var id_bottoni=["#mostra1","#mostra2","#mostra3","#mostra4","#mostra5","#mostra6","#mostra7","#mostra8"];
				var tipi=["hasAuthor","hasPublicationYear","hasDOI","hasTitle","hasComment","denotesRhetoric","cites","hasURL"];
				var classi=["autore","data","DOI","titolo","commento","retorica","citazione","url"];

				var gruppo=(esempio_endpoint_gruppo.substring(esempio_endpoint_gruppo.length-7,esempio_endpoint_gruppo.length));
				visualizza_annotazioni(data,id_bottoni,tipi,classi,gruppo);

				//console.log(JSON.stringify(lista_json_gruppi[lista_json_gruppi.length-1]));
				mostra_nascondi_circoletto = mostra_nascondi_circoletto + 1; //ho controllato questo gruppo
				//console.log(mostra_nascondi_circoletto);
				becker(mostra_nascondi_circoletto);
			},
			error: function() {
				mostra_nascondi_circoletto = mostra_nascondi_circoletto + 1;
				becker(mostra_nascondi_circoletto);
			}
		});
	}
	//se sto esaminando il nostro
	else {
		$.ajax({
			dataType: "jsonp",
			url:queryURL,
			type: "GET",
			//async: "false",
			success: function(data) {
				console.log("La query del nostro grafo restituisce",JSON.stringify(data.results.bindings));
				//per ora faccio partire di default il nostro scraper, quando avremo la certezza
				//che il nostro sparql funziona leveremo i commenti
				/*console.log("per ora faccio partire il nostro scraper comunque");
				scraper_automatico(url_doc_visual);*/
				if(data.results.bindings.length==0) {
					console.log("Nel nostro sparql non c'e niente, chiamo lo scraper");
					scraper_automatico(url_doc_visual);
				}
				else {
					
					//document.write(JSON.stringify(data.results.bindings));
					data=converti_in_nostro_json(data.results.bindings);
					//document.write(JSON.stringify(data));	
					lista_json.push(data);
					ultimo=ultimo+1;

					var k;
			   		for(k=0;k<lista_json[ultimo].annotations.length;k++) {
			   			lista_json[ultimo].annotations[k].salvata=0;
			   		}

					console.log("Json convertito=",JSON.stringify(data));

					var id_bottoni=["#mostra1","#mostra2","#mostra3","#mostra4","#mostra5","#mostra6","#mostra7","#mostra8"];
					var tipi=["hasAuthor","hasPublicationYear","hasDOI","hasTitle","hasComment","denotesRhetoric","cites","hasURL"];
					var classi=["autore","data","DOI","titolo","commento","retorica","citazione","url"];

					var gruppo=(esempio_endpoint_gruppo.substring(esempio_endpoint_gruppo.length-7,esempio_endpoint_gruppo.length));
					visualizza_annotazioni(data,id_bottoni,tipi,classi,gruppo);

					//console.log(JSON.stringify(lista_json_gruppi[lista_json_gruppi.length-1]));
					console.log("Quste sono le annotazioni del gruppo",esempio_endpoint_gruppo);
					mostra_nascondi_circoletto = mostra_nascondi_circoletto + 1; //ho controllato questo gruppo
					//console.log(mostra_nascondi_circoletto);
					becker(mostra_nascondi_circoletto);
				}

			},
			error: function() {
				mostra_nascondi_circoletto = mostra_nascondi_circoletto + 1;
				becker(mostra_nascondi_circoletto);
			}
		});
	}
}
function converti_in_nostro_json(json) {
	
	var i,json_convertito={
		annotations: [],
		"provenance": {
        	"author": {
            	"name": "",
            	"email": ""
        	},
        	"time": ""
    	}
	};
	
	//non è definitivo, da risolvere
	for(i=0;i<json.length;i++) {
		//scarto le annotazioni sul documento, le nostre non dovrebbero venire scartate perchè hanno questi campi vuoti, non undefined
		if((json[i].xpath.value!="document")&&(!(json[i].start===undefined))&&(!(json[i].end===undefined))) {
			var j_push= {
				"type": "",
				"label": "",
		        "body": {
		            "subject": "",
		            "predicate": "",
		            "object": "",
		            "resource":{
		            	"id": ""
		            },
		            "label":""
		        },
		        "target": {
		            "source": "",
		            "id": "",
			        "start": "",
		            "end": ""
		        }
			};

			//parte tipo
			var tipo=json[i].prd.value;
			//console.log(tipo);
			tipo=tipo.split("/");
			var scorr;
			for(scorr=0;scorr<tipo.length;scorr++) {
				//sposto scorr fino all'ultima parola
			}
			tipo=tipo[scorr-1];
			//console.log(tipo);
			if(tipo.indexOf("#")>-1) {
				tipo=tipo.split("#");
				for(scorr=0;scorr<tipo.length;scorr++) {
					//sposto scorr fino all'ultima parola
				}
				tipo=tipo[scorr-1];
			}
			//console.log("TIPO",tipo);
			
			if((tipo.indexOf("title")>-1)||(tipo.indexOf("hasTitle")>-1)) {
				tipo="hasTitle";
			} 
			else {
				if((tipo.indexOf("doi")>-1)||(tipo.indexOf("hasDOI")>-1)) {
					tipo="hasDOI";
				}
				else {
					if((tipo.indexOf("creator")>-1)||(tipo.indexOf("hasAuthor")>-1)||(tipo.indexOf("author")>-1)) {
						tipo="hasAuthor";
					}
					else {
						if((tipo.indexOf("denotes")>-1)||(tipo.indexOf("denotesRhetoric")>-1)||(tipo.indexOf("rhetoric")>-1)||(tipo.indexOf("retorica")>-1)) {
							tipo="denotesRhetoric";
						}
						else {
							if((tipo.indexOf("comment")>-1)||(tipo.indexOf("hasComment")>-1)) {
								tipo="hasComment";
							}
							else{
								if((tipo.indexOf("url")>-1)||(tipo.indexOf("hasURL")>-1)) {
									tipo="hasURL";
								}
								else if((tipo.indexOf("data")>-1)||(tipo.indexOf("hasPublicationYear")>-1)||(tipo.indexOf("PublicationYear")>-1)) {
									tipo="hasPublicationYear";
								}
								else if((tipo.indexOf("cites")>-1)) {
									tipo="cites";
								}
							}
						}
					}
				}
			}

			j_push.type=tipo;
			//fine tipo
			
			//questo campo per ora è commentato, ma va messo
			if((json[i].label===undefined)||(json[i].label.value)===undefined) {
				if(tipo=="hasTitle") j_push.label="Titolo";
				if(tipo=="hasAuthor") j_push.label="Autore";
				if(tipo=="hasDOI") j_push.label="DOI";
				if(tipo=="hasPublicationYear") j_push.label="Data";
				if(tipo=="cites") j_push.label="Citazione";
				if(tipo=="denotesRhetoric") j_push.label="Retorica";
				if(tipo=="hasComment") j_push.label="Commento";
				if(tipo=="hasURL") j_push.label="Url";
			}
			else j_push.label=json[i].label.value;
			//console.log(j_push.label);

			//parte per il subject
		
			//parte predicate
		
			//parte object
			try {
				var ogg;
				if(tipo=="hasAuthor") {
					ogg=json[i].obj.value;
					ogg=ogg.split("/");
					ogg=ogg[ogg.length-1];
				}
				else {
					if(tipo=="hasTitle") {
						ogg=json[i].obj.value;
					}
					else {
						if(tipo=="hasDOI") {
							ogg=json[i].obj.value;
						}
						else {
							if(tipo=="hasPublicationYear") {
								ogg=ogg=json[i].obj.value;
							}
							else {
								if(tipo=="cites") {
									ogg=json[i].obj.value;
									ogg=ogg.split("/");
									ogg=ogg[ogg.length-1];
								}
								else {
									if(tipo=="denotesRhetoric") {
										ogg=json[i].obj.value;
									}
									else {
										if(tipo=="hasComment") {
											ogg=json[i].obj.value;
										}
										else {
											if(tipo=="hasURL") {
												ogg=url_doc_visual;
											}
										}
									}
								}
							}
						}
					}
				}

				j_push.body.object=ogg;
			}
			catch(e) {

			}

			
			//parte resource
			try {
				if((json[i].obj!=undefined)&&(json[i].obj.value!=undefined)) {
					var aut;
					aut=json[i].obj.value;
					aut=aut.split("/");
					aut=aut[aut.length-1];
					aut=aut.replace("rsch:","");
					if(aut.length>3) {
						j_push.body.resource.id=aut;
					}
				}
			}
			catch(e) {

			}

			j_push.target.id=json[i].xpath.value;
			j_push.target.start=parseInt(json[i].start.value);
			j_push.target.end=parseInt(json[i].end.value);
			

			json_convertito.annotations.push(j_push);
			//parte per name
			var nome=json[i].who.value; 
			nome=nome.split("/");
			var mov;
			for(mov=0;mov<nome.length;mov++) {
				//sposto mov fino all'ultima parola
			}
			nome=nome[mov-1];
			json_convertito.provenance.author.name=nome;
			//fine parte name

			//parte email

			if((json[i].when.value===undefined)||(json[i].when===undefined)) json_convertito.provenance.time="time";
			else json_convertito.provenance.time=json[i].when.value;
		}
	}
	//console.log("Il json convertito e'",JSON.stringify(json_convertito));
	//document.write(JSON.stringify(json_convertito));
	return(json_convertito);
}
function conversione_turtle(json){

	json=vari_cari_di_rimuovi_caratteri_turtle(json);
	

	var pass = JSON.stringify(json);
	var url = url_doc_visual;
	//console.log ("==========================================================");
	//console.log(pass);
	var i=0;
	for (i=0; i<json.annotations.length; i++){

		$.ajax({
			url:"core/lib/annotation_manager/insert_graph.php",
			method: "POST",
			data: {mydata:pass, indice:i, u:url},
			dataType: "text",
			success: function(data){
				//alert("Hai salvato le annotazioni da te create");
				//console.log("Le annotazioni automatiche sono state salvate? "+data); //decommentare per vedere il turtle inviato
				console.log("Ho creato il turtle dell'annotazione in questione");
			},
			error: function(jqXHR,textStatus,textError){
				console.log("Errore salvataggio server, funzione conversione_turtle: " +textStatus + " " + textError);
    		}
		});
	}
}

function filtra_gruppi(gruppo) {
	//console.log($(document.getElementById("risultato")).html());
	var id="#"+gruppo.id;
	var classe="."+gruppo.id;
	//console.log(gruppo,id);
	//checkbox attiva
	if($(id).hasClass("btn-primary")) {
		console.log("mostro quelle del gruppo "+gruppo.id);
		$(document.getElementById("risultato")).find($(".cera-"+gruppo.id)).removeClass("cera-"+gruppo.id).addClass(gruppo.id);
		var oggettone=$(document.getElementById("risultato")).find($("."+gruppo.id));
		var i;
		for(i=0;i<oggettone.length;i++) {
			var stringa=oggettone[i].outerHTML;
			var j,cla;
			for(j=stringa.indexOf("cera_");(j<stringa.length)&&(stringa.charAt(j)!=String.fromCharCode(32));j++) {

			}
			cla=stringa.substring(stringa.indexOf("cera_")+5,j);
			$($(document.getElementById("risultato")).find($("."+gruppo.id))[i]).removeClass("cera_"+cla).addClass("colore_"+cla);
		}
		$(id).removeClass("btn-primary").addClass("btn-info");
	}
	//checkbox disattivata
	else {
		if($(id).hasClass("btn-info")) {
			console.log("nascondo quelle del gruppo "+gruppo.id);
			$(document.getElementById("risultato")).find($(classe)).removeClass(gruppo.id).addClass("cera-"+gruppo.id);
			var ceraclasse=".cera-"+gruppo.id;
			//console.log(ceraclasse);
			//oggettone contiene tutti gli span che avevano la classe del gruppo in questione
			var oggettone=$(document.getElementById("risultato")).find($(ceraclasse));
			//console.log(oggettone);
			var i;
			for(i=0;i<oggettone.length;i++) {
				var stringa=oggettone[i].outerHTML;
				var conto=0;
				//console.log(stringa);
				//parto da 1 perchè se ltw fosse a 0 sicuramente non ci sarebbe un trattino prima
				if(stringa.indexOf("ltw")>1) {
					//se trovo un ltw controllo il carattere precedente alla l, se non è un trattino vuol dire
					//che l'annotazione è anche di un altro gruppo oltre quello in esame
					if(stringa.charAt(stringa.indexOf("ltw")-1)!="-") {
						conto++;
					}
				}
				//se conto è rimasto 0, vuol dire che l'annotazione era solo del gruppo in questione e va nascosta
				//le istruzioni che scrivo non tengono conto del fatto che il frammento può avere più classi: non saprei
				//quale eliminare
				if(conto==0) {
					var cla;
					var j;
					//mi serve per trovare la classe
					for(j=stringa.indexOf("colore_");(j<stringa.length)&&(stringa.charAt(j)!=String.fromCharCode(32));j++) {

					}
					cla=stringa.substring(stringa.indexOf("colore_")+7,j);
					$($(document.getElementById("risultato")).find($(ceraclasse))[i]).removeClass("colore_"+cla).addClass("cera_"+cla);

				}
			}
			$(id).removeClass("btn-info").addClass("btn-primary");
		}
		else console.log("Il gruppo selezionato non ha annotazioni su questo documento");
	}
	//console.log($(document.getElementById("risultato")).html());
}

function risolvi_immagine(documento) {
	var k;
	var provenienza=url_doc_visual;
	for(k=0;k<$(documento).find("img").length;k++) {
		var stringa=$($(documento).find("img")[k]).attr("src");
		var prov_split=provenienza.split("/");
		provenienza=provenienza.replace(prov_split[prov_split.length-1],stringa);
		$($(documento).find("img")[k]).attr("src",provenienza);
	}
	return(documento);
}

function nascondi_annotazioni_altri_gruppi() {
	var i;
	$.each($(document.getElementById("risultato")).find("span"),function(k,v){
		var stringa=$(v).attr("class");
		//se c'è un ltw ma non è cera-ltw
		if((stringa!=undefined)&&(stringa.length!=0)) {
			if((stringa.indexOf("ltw")>-1)&&(stringa.indexOf("cera-")<0)) {
				var cla=stringa.substring(stringa.indexOf("ltw"),stringa.indexOf("ltw")+7);
				if(cla!="ltw1511") {
					var j;
					//mi serve per trovare il colore
					for(j=stringa.indexOf("colore_");(j<stringa.length)&&(stringa.charAt(j)!=String.fromCharCode(32));j++) {

					}
					var col=stringa.substring(stringa.indexOf("colore_")+7,j);					
					$(v).removeClass("colore_"+col).addClass("cera_"+col);
					$(v).removeClass(cla).addClass("cera-"+cla);
					$("#"+cla).removeClass("btn-info").addClass("btn-primary");
				}
			}
		}
	});
}

function cancella_visualizzazione_nostra_annotazione(stringa,tipo) {
	$(stringa).removeClass("ltw1511").addClass("cancellatogruppo");
	var colore="colore_"+tipo;
	$(stringa).removeClass(colore).addClass("cancellatocolore");
	return(stringa);
}

function associa_label_a_json(json,id) {
	var k;
	for(k=0;(lista_json[ultimo].annotations[k]!=json)&&(k<lista_json[ultimo].annotations.length);k++) {
		
	}
	try {
		lista_json[ultimo].annotations[k].identificatore=id;
	}
	catch (err) {
		return(0);
	}
	//console.log(JSON.stringify(lista_json[ultimo].annotations));
}
function becker(alla_gastronomia_serviamo_il_numero){
	if(alla_gastronomia_serviamo_il_numero < 37) { //37 e' un numero un po empirico
		/*$('#risultato').hide();
		$('.container_progresso').show();*/
		//$(".container_progresso").css("z-index","-1");

		cliccabile=1;
		//console.log(alla_gastronomia_serviamo_il_numero);
	}
	else{
		cliccabile=0;
		/*$('#risultato').show();
		$('.container_progresso').hide();*/
		//console.log("hide");
		$(".container_progresso").hide();

		//se siamo dentro ad annotator, al cambiamento del documento in ogni caso le annotazioni degli altri van cancellate
		if(is_logged==true) {
			//nascondi_annotazioni_altri_gruppi();
		}
		aggiusta_citazione();


		if(is_logged==false) {
			$(".modifica_ann").hide();
			$(".cancella_ann").hide();

			$("span").css("cursor","pointer");
		}
		else {
			$(".modifica_ann").show();
			$(".cancella_ann").show();

			$("span").css("cursor","");
		}

		
		//console.log(alla_gastronomia_serviamo_il_numero);
		console.log("Elenco autori disponibili ",elenco_autori_doc_visualizzato);
	}
}

function salva_tutto() {
	//var json = lista_json[ultimo];
		lista_json[ultimo]=vari_cari_di_rimuovi_caratteri_turtle(lista_json[ultimo]);

		var pass = JSON.stringify(lista_json[ultimo]);
		var url = url_doc_visual;
		//console.log (pass);
		//console.log(url);
		var i=0;
		for (i=0; i<lista_json[ultimo].annotations.length; i++){

			if(lista_json[ultimo].annotations[i].salvata==1) {
				$.ajax({
					url:"core/lib/annotation_manager/insert_graph.php",
					method: "POST",
					data: {mydata:pass, indice:i, u:url},
					dataType: "text",
					success: function(data){
						//alert("Hai salvato le annotazioni da te create");
						//console.log("Le annotazioni automatiche sono state salvate? "+data); //decommentare per vedere il turtle inviato

						console.log("L'annotazione è stata salvata");
					},
					error: function(jqXHR,textStatus,textError){
						console.log("Errore salvataggio annotazione, funzione salva_tutto " +textStatus + " " + textError);
		    		}
				});
				//questo assegnamento andrebbe fatto dentro alla success,
	    		//ma se lo facessi in quel momento dovrei andarmi a ricercare l'annotazione in questione
	    		//mettendo questa istruzione qui, si da per scontato che l'inserimento sia andato a buon fine
	    		lista_json[ultimo].annotations[i].salvata=0;

			}
			else console.log("L'annotazione in questione è già stata salvata");
		}
		$(".fraseee").text("Salvata");
		tutto_salvato=0;
}

function cancella_tutto() {
	$.ajax({
		url: "core/lib/annotation_manager/svuota_grafo.php",
		method: "GET",
		dataType: "text",
		success: function(data){
			alert("Grafo Svuotato? "+data);
			var k;
	   		for(k=0;k<lista_json[ultimo].annotations.length;k++) {
	   			lista_json[ultimo].annotations[k].salvata=1; //cosi e' non salvata 
	   		}
	   		$(".fraseee").text("Non salvata");
	   		tutto_salvato=1;
		},
		error: function(){
			alert("cazzone " +textStatus + " " + textError);
		}
	});
}

function gestisci_autori(json) {
	try {
		if(json.body!=undefined) {
			if(json.body.resource!=undefined) {
				if(json.body.resource.id!=undefined) {
					var stringa=json.body.resource.id;
					stringa=stringa.split("/");
					var autore=stringa[stringa.length-1];
					if(elenco_autori_doc_visualizzato.indexOf(autore)<0) {
						elenco_autori_doc_visualizzato.push(autore);
					}
				}
			}
		}
	}
	catch(e) {
		console.log("non sono riuscito ad ottenere il nome dell'autore");
	}
}

function aggiusta_citazione() {
	//da errori per le citazioni in mezzo al testo
	$("#risultato span").each(function(){
		//quando sono su una citazione la scorro fino in basso per eliminare il colore degli span che non sono citazioni
		if($(this).hasClass("colore_citazione")) {
			var giu;
			for(giu=$(this).children();(giu.is("span"))&&(!(giu.hasClass("colore_citazione")));giu=giu.children()) {
				try {
					var colore1=giu.attr("class");
					
				
					var col;

					if(colore1.indexOf("colore")>-1) {
						col=trova_colore(colore1,0);
					}
					else {
						col=trova_colore(colore1,1);
					}	

					
					giu.removeClass("colore_"+col).addClass("sottocampo!"+col);
				}
				catch(e) {
					console.log("Errore aggiusta_citazione giu");
					//console.log(e);
				}
			}
			
		}
		/*else {
			//per sicurezza, quando incontro uno span che non è citazione, mi assicuro che il padre non sia una citazione, altrimenti ne rimuovo il colore
			if(($(this).hasClass("colore_autore"))||($(this).hasClass("colore_data"))||($(this).hasClass("colore_DOI"))||($(this).hasClass("colore_titolo"))||($(this).hasClass("colore_retorica"))||($(this).hasClass("colore_commento"))||($(this).hasClass("colore_url"))) {
				var su,prim=$(this);
				for(su=$(this).parent();su.is("span");su=su.parent()) {
					try {
						var colore1=prim.attr("class");
						
						var colore2=$(prim.context).attr("class");
						var col;

						if((colore1.indexOf("colore")>-1)||(colore2.indexOf("colore")>-1)) {
							col=trova_colore(colore1,colore2,0);
						}
						else {
							col=trova_colore(colore1,colore2,1);
						}	

						
						if(su.hasClass("colore_citazione")) prim.removeClass("colore_"+col).addClass("sottocampo!"+col);
						prim=su;
					}
					catch(e) {
						console.log("Errore aggiusta_citazione su");
						//console.log(e);
					}
				}
			}
		}*/
		//istruzioni aggiuntive per i sottocampi della citazione per gestire span con più classi (wtf!?)
		try {
			var assicurazione=$(this),trovato=0;
			assicurazione=assicurazione.attr("class");
			while((assicurazione.indexOf("colore")>-1)&&(assicurazione.indexOf("sottocampo")>-1)) {
				assicurazione=assicurazione.replace("colore","");
				trovato++;
			}
			if(trovato>0) {
				var colore1=$(this).attr("class");
					
				
				var col;

				if(colore1.indexOf("colore")>-1) {
					col=trova_colore(colore1,0);
				}
				else {
					col=trova_colore(colore1,1);
				}	

				
				$(this).removeClass("colore_"+col).addClass("sottocampo!"+col);
				console.log("Gestione span aggiuntiva fatta");
			}
		}
		catch(e) {
			console.log("Gestione span con più classi fallita");
		}

	});
}

function parsa_i_numeri(json) {
	json.target.start=parseInt(json.target.start);
	json.target.end=parseInt(json.target.end);
	return(json);
}

function trova_colore(el1,s) {
	//valori originali degli elementi
	var or1=el1,ritorna;
	if(s==0) {
		try {
			el1=or1.split(" ");
			var k;
			for(k=0;k<el1.length;k++) {
				el1=el1[k];
				el1=el1.split("_");
				ritorna=el1[1];
				if((ritorna=="autore")||(ritorna=="titolo")||(ritorna=="DOI")||(ritorna=="data")||(ritorna=="citazione")||(ritorna=="url")||(ritorna=="commento")||(ritorna=="retorica")) {
					return(ritorna);
				}
				el1=or1.split(" ");
			}

			return("TIPO1")
		}
		catch(e) {
			return("TIPO2");
		}
	}
	else {
		try {

			el1=or1.split(" ");
			var k,ritorna;
			for(k=0;k<el1.length;k++) {
				el1=el1[k];
				el1=el1.split("!");
				ritorna=el1[1];
				if((ritorna=="autore")||(ritorna=="titolo")||(ritorna=="DOI")||(ritorna=="data")||(ritorna=="citazione")||(ritorna=="url")||(ritorna=="commento")||(ritorna=="retorica")) {
					return(ritorna);
				}
				el1=or1.split(" ");
			}

			return("TIPO3");
		}
		catch(e) {
			return("TIPO4");
		}
	}
	
}



function vari_cari_di_rimuovi_caratteri_turtle(json) {
	var kk;
	for(kk=0;kk<json.annotations.length;kk++) {
		try {
			if(json.annotations[kk].type=="hasAuthor") {
				json.annotations[kk].body.label=rimuovi_apici_turtle(json.annotations[kk].body.label);
				json.annotations[kk].body.resource.label=rimuovi_apici_turtle(json.annotations[kk].body.resource.label);
				json.annotations[kk].body.resource.id = check_iri_autore(json.annotations[kk].resource.id);
			}
			else {
				if((json.annotations[kk]=="hasComment")||(json.annotations[kk]=="denotesRhetoric")) {
					json.annotations[kk].body.object=rimuovi_apici_turtle(json.annotations[kk].body.object);
					json.annotations[kk].label=rimuovi_apici_turtle(json.annotations[kk].label);
				}
			
				else {
					if(json.annotations[kk].type=="cites") {
						json.annotations[kk].label=rimuovi_apici_turtle(json.annotations[kk].label);
						json.annotations[kk].body.resource.label=rimuovi_apici_turtle(json.annotations[kk].body.resource.label);
					}
					else {
						json.annotations[kk].body.object=rimuovi_apici_turtle(json.annotations[kk].body.object);
					}
				}
			}
		}
		catch(e) {

		}
	}
	return(json);
}

function aggiusta_xpath1(stringa) {
		var spez,part,i=0,part2;
		if((stringa.charAt(0)=="/")&&(stringa.charAt(1)=="/")) spez=stringa.substring(2,stringa.length);
		else spez=stringa.substring(0,stringa.length);
		spez=spez.split("/");
		part="\/\/"+spez[i];
		part2=part;

		try{
			var testa=$(document.getElementById("risultato")).xpath(part);
		}
		catch(err){
			return(0);
		}

		while(!($(document.getElementById("risultato")).xpath(part).html()===undefined)) {

			try{
				var testa=$(document.getElementById("risultato")).xpath(part);
			}
			catch(err){
				return(0);
			}

			part2=part;
			i++;
			part=part+"/"+spez[i];			
		}
		part2=part2+"/*/"+spez[i];



		try{
			var testa=$(document.getElementById("risultato")).xpath(part2);
		}
		catch(err){
			console.log("Errore in aggiustamento");
			return(0);
		}

		if($(document.getElementById("risultato")).xpath(part2).html()===undefined) {		
			/*console.log(stringa,"inizialmente");
			console.log(prim,"dopo 1");
			console.log(part2,"dopo 2");*/
			//console.log(part2);
			//console.log(stringa);
			console.log("Il selettore non e' valido, nonostante sia stato convertito:");
			return(0);
		}
		else {
			//devo aggiungere la parte destra della stringa ma non ho alcuna sicurezza che sia corretta
			//quindi rifaccio un controllo e nel caso non lo sia mi arrendo
			var agg;
			for(agg=i+1;agg<spez.length;agg++) {
				part2=part2+"/"+spez[agg];
			}

			try{
				var testa=$(document.getElementById("risultato")).xpath(part2);
			}
			catch(err){
				console.log("Errore in aggiustamento");
				return(0);
			}

			//console.log($(document.getElementById("risultato")).xpath(part2).html());
			if($(document.getElementById("risultato")).xpath(part2).html()===undefined) {
				console.log("Selettore non valido per aggiustamento finale fallito");
				return(0);
			}
			//console.log("Ho tentato una conversione del selettore, forse funziona");
			//console.log("xpath2= ",part2);
			else return(part2);
		}



		//non credo arrivi mai qui
		return(part2);
}
function controlla_se_lista(ind) {
	var k,trovato=0,nome;
	for(k=0;k<flight.length;k++) {
		if((flight[k][1]).indexOf(ind)>-1) trovato=1;
	}
	if(trovato==0) {
		try {
			nome=ind.split("/");
			nome=nome[2];
			nome=nome.split(".");
			nome=nome[1];
		}
		catch(e) {
			nome=ind;
		}
		flight[k]=[];
		flight[k][0]=nome;
		flight[k][1]=ind;
		$("#doc_list_din").append('<li><a onclick=\"do_the_click(name)\" href=\"javascript:void(0);\" name=\"'+flight[k][1]+'\">'+flight[k][0]+'</a></li>');
		
	}
}