<?php
	require_once 'SparqleParkle_lib.php';
	require_once 'insert_graph.php';
	$json = json_decode($_POST['mydata']);
	$indice = $_POST['indice'];
	$url = $_POST['u'];

	//prima cancello tutto il grafo poi richiamo la transformer che sta nell'altro file insert_graph e poi la inserisci
	prima_cancello_tutto();
	//ora richiamo i metodi che usavo in insert_graph
	$trasformato = transformers($json, $indice, $url);
	insertAnnotation($trasformato);

	function prima_cancello_tutto(){
		$deleteQuery='CLEAR GRAPH <http://vitali.web.cs.unibo.it/raschietto/graph/ltw1511>';
		sparql_insert_delete($deleteQuery, "ltw1511", "gdXsah91L");
	}
?>