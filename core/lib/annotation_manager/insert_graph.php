<?php
require_once('SparqleParkle_lib.php');
//ricavo il mio json inviato dalla chiamata ajax
$json = json_decode($_POST['mydata']);
$indice = $_POST['indice'];
$url = $_POST['u'];

//lo trasformo in turtle
$trasformato = transformers($json, $indice, $url);
insertAnnotation($trasformato);
function transformers($json, $indice, $url){

  //costruzione turtle caso per caso
  if($json->annotations[$indice]->type == "hasAuthor"){
    $label1 = "Autore";
    $id = trasforma_xpath($json->annotations[$indice]->target->id);

    $turtle = "[] a oa:Annotation ; rdfs:label ".'"'.$json->annotations[$indice]->label.'"'.";
                    oa:hasTarget[
                    a oa:SpecificResource;
                    oa:hasSource ".'<'.$url.'>'." ; 
                    oa:hasSelector [ a oa:FragmentSelector ;
                          rdf:value ".'"'.$json->annotations[$indice]->target->id.'"'." ;
                          oa:start ".'"'.$json->annotations[$indice]->target->start.'"'."^^xsd:nonNegativeInteger ;
                          oa:end ".'"'.$json->annotations[$indice]->target->end.'"'."^^xsd:nonNegativeInteger ] ] ; ;
                    oa:hasBody _:author ;
                    oa:annotatedBy <mailto:ltw1511@studio.unibo.it> ;
                    oa:annotatedAt ".'"'.$json->provenance->time.'"'." .

                    _:author a rdf:Statement ; rdf:subject ".'<'.$json->annotations[$indice]->body->subject.'>'." ;
                    rdf:predicate dcterms:creator ; rdf:object ".'<'.togli_spazietti($json->annotations[$indice]->body->resource->id).'>'." ;
                    rdfs:label ".'"'.$json->annotations[$indice]->body->label.'"'." .
                    rsch:type rdfs:label ".'"'.$json->annotations[$indice]->body->resource->label.'"'." .
                    <mailto:ltw1511@studio.unibo.it> foaf:name ".'"'.$json->provenance->author->name.'"'." ;
                    schema:email ".'"'.$json->provenance->author->email.'"'." .";
    return($turtle);
  }
  else{
    if($json->annotations[$indice]->type == "hasPublicationYear"){
      $label1 = "Anno di Pubblicazione ".$json->annotations[$indice]->body->object;
      $label2 = "Anno di Pubblicazione ".$json->annotations[$indice]->body->object;
      $obj = "anno";                    
      $id = trasforma_xpath($json->annotations[$indice]->target->id);

      $turtle = "[]a oa:Annotation ;
                      rdfs:label ".'"'.$label1.'"'."^^xsd:string ;
                      rsch:type ".'"'.$json->annotations[$indice]->type.'"'."^^xsd:string;
                      oa:annotatedAt ".'"'.$json->provenance->time.'"'."^^xsd:dateTime;
                      oa:annotatedBy <mailto:ltw1511@studio.unibo.it>;
                      oa:hasBody [ a rdf:Statement ;
                                  rdfs:label ".'"'.$label2.'"'."^^xsd:string ;
                                  rdf:object <stringaTestuale> ;
                                  rdf:predicate fabio:hasPublicationYear ;
                                  rdf:subject <".$json->annotations[$indice]->body->subject.">] ;
                      oa:hasTarget [a oa:SpecificResource ;
                                    oa:hasSelector [ a oa:FragmentSelector;
                                                    rdf:value  ".'"'.$id.'"'."^^xsd:string ; 
                                                    oa:end ".$json->annotations[$indice]->target->end." ;
                                                    oa:start ".$json->annotations[$indice]->target->start."] ;
                                    oa:hasSource <".$url."> ].
                                  ";
      return $turtle;
      }
      else{
        if($json->annotations[$indice]->type == "hasTitle"){
          $id = trasforma_xpath($json->annotations[$indice]->target->id);
          $label2 = "Titolo: ".togli_spazietti($json->annotations[$indice]->body->object);

          $turtle = "[] a oa:Annotation ;  
          rdfs:label ".'"'.$json->annotations[$indice]->label.'"'.";
          oa:hasTarget [
          a oa:SpecificResource;
          oa:hasSource ".'<'.$url.'>'." ;
          oa:hasSelector [ a oa:FragmentSelector ;
            rdf:value ".'"'.$id.'"'." ; 
            oa:start ".'"'.$json->annotations[$indice]->target->start.'"'."^^xsd:nonNegativeInteger ;
            oa:end ".'"'.$json->annotations[$indice]->target->end.'"'."^^xsd:nonNegativeInteger ] ] ; ; 
          oa:hasBody _:title ;
          oa:annotatedBy <mailto:ltw1511@studio.unibo.it> ; 
          oa:annotatedAt ".'"'.$json->provenance->time.'"'." .
          _:title a rdf:Statement ; rdfs:label ".'"'.$label2.'"'."^^xsd:string ; rdf:subject ".'<'.$json->annotations[$indice]->body->subject.'>'." ;
          rdf:predicate dcterms:title ; rdf:object ".'"'.$json->annotations[$indice]->body->object.'"'."^^xsd:string .
          <mailto:ltw1511@studio.unibo.it> foaf:name ".'"'.$json->provenance->author->name.'"'." ;
          schema:email ".'"'.$json->provenance->author->email.'"'." .";

          return $turtle;
        }
        else{
          if($json->annotations[$indice]->type == "hasDOI"){
            $id = trasforma_xpath($json->annotations[$indice]->target->id);
            $label2 = "DOI: ".togli_spazietti($json->annotations[$indice]->body->object);

            $turtle = "[] a oa:Annotation ;  
            rdfs:label ".'"'.$json->annotations[$indice]->label.'"'.";
            oa:hasTarget [
            a oa:SpecificResource;
            oa:hasSource ".'<'.$url.'>'." ;
            oa:hasSelector [ a oa:FragmentSelector ;
              rdf:value ".'"'.$id.'"'." ; 
              oa:start ".'"'.$json->annotations[$indice]->target->start.'"'."^^xsd:nonNegativeInteger ;
              oa:end ".'"'.$json->annotations[$indice]->target->end.'"'."^^xsd:nonNegativeInteger ] ] ; ; 
            oa:hasBody _:doi ;
            oa:annotatedBy <mailto:ltw1511@studio.unibo.it> ; 
            oa:annotatedAt ".'"'.$json->provenance->time.'"'." .
            _:doi a rdf:Statement ; rdfs:label ".'"'.$label2.'"'."^^xsd:string ; rdf:subject ".'<'.$json->annotations[$indice]->body->subject.'>'." ;
            rdf:predicate prism:doi ; rdf:object ".'"'.$json->annotations[$indice]->body->object.'"'."^^xsd:string .
            <mailto:ltw1511@studio.unibo.it> foaf:name ".'"'.$json->provenance->author->name.'"'." ;
            schema:email ".'"'.$json->provenance->author->email.'"'." .";

            return $turtle;
          }
          else{
           if($json->annotations[$indice]->type == "hasURL" || $json->annotations[$indice]->type == "hasUrl"){
              $label1 = "Indirizzo";
              $id = trasforma_xpath($json->annotations[$indice]->target->id);
              $subj = str_replace('.html', '_ver1', $url);
              $obj = "url";

              $turtle = "[]a oa:Annotation ;
                              rdfs:label ".'"'.$label1.'"'."^^xsd:string ;
                              rsch:type ".'"'.$json->annotations[$indice]->type.'"'."^^xsd:string;
                              oa:annotatedAt ".'"'.$json->provenance->time.'"'."^^xsd:dateTime ;
                              oa:annotatedBy <mailto:ltw1511@studio.unibo.it> ;
                              oa:hasBody [ a rdf:Statement ;
                                          rdfs:label ".'"Url di questo articolo: '.$url.'"'."^^xsd:string ;
                                          rdf:object <string> ;
                                          rdf:predicate fabio:hasURL ;
                                          rdf:subject <".$subj."> ] ;
                              oa:hasTarget [a oa:SpecificResource ;
                                            oa:hasSelector [ a oa:FragmentSelector ;
                                                            rdf:value  ".'"'.$id.'"'."^^xsd:string ; 
                                                            oa:end ".$json->annotations[$indice]->target->end." ;
                                                            oa:start ".$json->annotations[$indice]->target->start."] ;
                                            oa:hasSource <".$url."> ].
                                          ";
              return $turtle;
            }
            else{
              if($json->annotations[$indice]->type == "hasComment"){
                $label1 = "Commento";
                $obj = "xsd:string";                      
                $id = trasforma_xpath($json->annotations[$indice]->target->id);
                $subj = $url .'#'. $id;

                $turtle = "[]a oa:Annotation ;
                                rdfs:label ".'"'.$label1.'"'."^^xsd:string ;
                                rsch:type ".'"'.$json->annotations[$indice]->type.'"'."^^xsd:string;
                                oa:annotatedAt ".'"'.$json->provenance->time.'"'."^^xsd:dateTime ;
                                oa:annotatedBy <mailto:ltw1511@studio.unibo.it> ;
                                oa:hasBody [ a rdf:Statement ;
                                            rdfs:label ".'"'.$json->annotations[$indice]->label.'"'."^^xsd:string ;
                                            rdf:object <".togli_spazietti($json->annotations[$indice]->body->object)."> ;
                                            rdf:predicate schema:comment ;
                                            rdf:subject <".$subj."> ] ;
                                oa:hasTarget [a oa:SpecificResource ;
                                              oa:hasSelector [ a oa:FragmentSelector ;
                                                              rdf:value  ".'"'.$id.'"'."^^xsd:string ; 
                                                              oa:end ".$json->annotations[$indice]->target->end." ;
                                                              oa:start ".$json->annotations[$indice]->target->start."] ;
                                              oa:hasSource <".$url."> ].
                                            ";
                return $turtle;
              }
              else{
                if($json->annotations[$indice]->type == "cites"){
                  $label1 = "Citazione";
                  $obj = $url."_ver1_cited";
                  $id = trasforma_xpath($json->annotations[$indice]->target->id);
                  $subj = str_replace('.html', '_ver1', $url);
                  $turtle = "[] a oa:Annotation ;
                                  rdfs:label ".'"'.$label1.'"'."^^xsd:string ;
                                  rsch:type ".'"'.$json->annotations[$indice]->type.'"'."^^xsd:string;
                                  oa:annotatedAt ".'"'.$json->provenance->time.'"'."^^xsd:dateTime ;
                                  oa:annotatedBy <mailto:ltw1511@studio.unibo.it> ;
                                  oa:hasBody [ a rdf:Statement ;
                                              rdfs:label ".'"Questo documento cita"'."^^xsd:string ;
                                              rdf:object <".$obj."> ;
                                              rdf:predicate cito:cites ;
                                              rdf:subject <".$subj."> ] ;
                                  oa:hasTarget [ a oa:SpecificResource ;
                                                  oa:hasSelector [ a oa:FragmentSelector ;
                                                                  rdf:value  ".'"'.$id.'"'."^^xsd:string ; 
                                                                  oa:end ".$json->annotations[$indice]->target->end." ;
                                                                  oa:start ".$json->annotations[$indice]->target->start." ] ;
                                                  oa:hasSource <".$url."> ].";
                  return $turtle;
                }
                else{
                    if($json->annotations[$indice]->type == "denotesRhetoric"){
                    $retorica = "Retorica";
                    $id = trasforma_xpath($json->annotations[$indice]->target->id);
                    $subj = $url .'#'. $id;

                    if($json->annotations[$indice]->label == 'Abstract'){
                      $obj = 'sro:Abstract';
                    }
                    else if($json->annotations[$indice]->label == 'Introduzione'){
                        $obj = 'deo:Introduction';
                    }
                    else if($json->annotations[$indice]->label == 'Materiali'){
                          $obj = 'deo:Materials';
                    }
                    else if($json->annotations[$indice]->label == 'Metodi'){
                            $obj = 'deo:Methods';
                    }
                    else if($json->annotations[$indice]->label == 'Discussione'){
                            $obj = 'sro:Discussion';
                    }
                    else{
                      $obj = 'sro:Conclusion';
                    }
                   
                    $turtle = "[]a oa:Annotation ;
                                        rdfs:label ".'"'.$retorica.'"'."^^xsd:string ;
                                        rsch:type ".'"'.$json->annotations[$indice]->type.'"'."^^xsd:string;
                                        oa:annotatedAt ".'"'.$json->provenance->time.'"'."^^xsd:dateTime ;
                                        oa:annotatedBy <mailto:ltw1511@studio.unibo.it> ; 
                                        oa:hasBody [ a rdf:Statement ;
                                                    rdfs:label ".'"'.$json->annotations[$indice]->label.'"'."^^xsd:string ;
                                                    rdf:object <".$obj."> ;
                                                    rdf:predicate sem:denotes ;
                                                    rdf:subject <".$subj."> ] ;
                                        oa:hasTarget [a oa:SpecificResource ;
                                                      oa:hasSelector [ a oa:FragmentSelector;
                                                                      rdf:value  ".'"'.$id.'"'."^^xsd:string ; 
                                                                      oa:end ".$json->annotations[$indice]->target->end." ;
                                                                      oa:start ".$json->annotations[$indice]->target->start."] ;
                                                      oa:hasSource <".$url."> ].
                                                    ";
                    return $turtle;
                  }
                  else{
                    $turtle = "";
                    return $turtle;
                  }
                }
              }
            }
          }   
        }
      }
    }
  }
  function insertAnnotation($turtle){
   $prefix ="prefix dcterms: <http://purl.org/dc/terms/>
    prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    prefix xsd: <http://www.w3.org/2001/XMLSchema#>
    prefix text: <http://purl.org/NET/mediatypes/text/>
    prefix application: <http://purl.org/NET/mediatypes/application/>
    prefix foaf: <http://xmlns.com/foaf/0.1/>
    prefix frbr: <http://purl.org/vocab/frbr/core#>
    prefix fabio: <http://purl.org/spar/fabio>
    prefix prism: <http://prismstandard.org/namespaces/basic/2.0/>
    prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    prefix sem: <http://www.ontologydesignpatterns.org/cp/owl/semiotics.owl#>
    prefix schema: <http://schema.org/>
    prefix oa: <http://www.w3.org/ns/oa#>
    prefix dlib: <http://www.dlib.org/dlib/march15/moulaison/>
    prefix rsch: <http://vitali.web.cs.unibo.it/raschietto/>
    prefix deo: <http://purl.org/spar/deo/>
    prefix cito: <http://purl.org/spar/cito/>
    prefix owl: <http://www.w3.org/2002/07/owl#>";


    $user = "ltw1511";
    $password = "gdXsah91L";
    //$GRAPH = "<http://localhost:3030/prova_persistente/>";
    $GRAPH = "<http://vitali.web.cs.unibo.it/raschietto/graph/ltw1511>";
    $all = $prefix.'INSERT DATA { GRAPH '.$GRAPH.'{'.$turtle.'}}'; //aggiungo i prefix al turtle e preparo la query per inserire
   
    //ricordare mail to nella costruzione del turtle
    $t = sparql_insert_delete($all, $user, $password); //per inserire il turtle creato
     if(isset($t)){
      echo ($all);
    }
    else{
      echo ('errore nella creazione del turtle');
    }
  }
  function trasforma_xpath($xpath){
    $xpath = str_replace('[', '', $xpath);
    $xpath = str_replace(']', '', $xpath);
    $xpath = str_replace('/','_', $xpath);
    return ($xpath);
  }
  function togli_spazietti($stringa){
    $stringa = str_replace(' ', '', $stringa);
    return ($stringa);
  }
?>