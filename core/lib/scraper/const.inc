<?php

/* xpath for statistica article content */
define('statistica_base_xpath', '//body/div[1]/div[2]/div[2]/div[3]');

/* xpath for dlib article content */
define('dlib_base_xpath', '//form[1]/table[3]/tr/td/table[5]/tr/td/table[1]/tr/td[2]');

/* xpath for almatourism article content */
define('almatourism_base_xpath', '//div/div[2]/div[2]/div[3]');

/* Xpath for article content from antropologia */
define('antropologia_base_xpath', '//div/div[2]/div[2]/div[3]');

/* xpath for generic page content */
define('generic_base_xpath', '//body');

?>
