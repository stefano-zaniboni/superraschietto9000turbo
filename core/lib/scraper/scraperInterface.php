<?php

/**
* This contains the methods that *each* scraper *must* define.
*/
interface scraperInterface{
  /**
  * Parses the loaded document and creates JSON for RDF annotations
  */
  public function parse();
  
  /**
  * Returns the name of the magazine that the document being parsed belongs to.
  * #returns string
  */
  public function getMagazineName();
  
}

?>