<?php
require_once "scraperBase.php";
require_once "almajournalsScraperBase.php";
require_once "scraperInterface.php";

/**
  * Scraper for almatourism, magazine available on almajournals
  */
  class almatourismScraper extends almajournalsScraperBase implements scraperInterface{
      /**
  * Returns the magazine name
  */
  public function getMagazineName() {
    return 'almatourism';
  }

  }

  ?>
