<?php
//<!--FUNZIONE IN PHP CHE MI RICAVA GLI DI TUTTI I GRUPPI, SI PARTE A CONTARE DA 1 NELL'ARRAY IN QUANTO NEL PRIMO POSTO C'E' UN ELEMENTO NULLO -->
require('simple_html_dom.php');
$html = file_get_html('http://vitali.web.cs.unibo.it/TechWeb15/ProgettoDelCorso');

$table = $html->find('table', 1);
$rowData = array();

foreach($table->find('tr') as $row) {
    // initialize array to store the cell data from each row
    $flight = array();
    foreach($row->find('th') as $cell) {
        // push the cell's text to the array
        $flight[] = $cell->plaintext;
    }
    $rowData[] = $flight;
}

$json = array();
foreach ($rowData as $row => $tr) 
{
	$json[] = $tr[1];
    $json[] = $tr[0];
}
echo json_encode($json);
?>
