<?php
/**
* Scraper for dlib magazine, issue 20
*/

require_once "almajournalsScraperBase.php";
require_once "scraperInterface.php";

class statisticaScraper extends AlmajournalsScraperBase implements scraperInterface{
  
  /**
  * Returns the magazine name
  */
  public function getMagazineName() {
    return 'statistica';
  }
  
}