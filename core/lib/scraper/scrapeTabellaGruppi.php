
<?php
require('simple_html_dom.php');
$html = file_get_html('http://vitali.web.cs.unibo.it/TechWeb15/ProgettoDelCorso');

$table = $html->find('table', 1);
$rowData = array();

foreach($table->find('tr') as $row) {
    // initialize array to store the cell data from each row
    $flight = array();
    foreach($row->find('th') as $cell) {
        // push the cell's text to the array
        $flight[] = $cell->plaintext;
    }
    $rowData[] = $flight;
}

echo '<table>';
foreach ($rowData as $row => $tr) {
    echo '<ol><tr>';
//    foreach ($tr as $td)
        echo '<td>'.'<li>'.$tr[0].'<input type="checkbox" name="my-checkbox" checked></li>'.'</td>';
    echo '</li></tr>';
}
echo '</table>';
?>
