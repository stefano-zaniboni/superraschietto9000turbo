<?php
/**
* Scraper for dlib magazine, issue 20
*/

require_once "scraperBase.php";
require_once "scraperInterface.php";

class dlibScraper extends scraperBase implements scraperInterface{
  var $url;

  public function __construct($article_url) {
    $this->loadDocument($article_url);
    $this->url = $article_url;
    $this->iri = $this->createDocumentIri($article_url);
  }

  public function parse() {
    $this->extractTitle();
    $this->extractDoi();
    $this->extractPublicationYear();
    $this->extractAuthor();
    $this->extractCitations();
    $this->extractUrl();
    $this->extractRhetoric();
    $this->setProvenance();
  }

  /**
  * Returns the magazine name
  */
  public function getMagazineName() {
    return 'dlib';
  }

  public function extractUrl() {
    $this->data['annotations'][] = array(
      'type' => 'hasUrl',
      'label' => 'Articolo disponibile all\'indirizzo',
      'body' => array(
        'subject' => $this->iri,
        'predicate' => 'fabio:hasUrl',
        'object' => $this->url,
      ),
      'target' => array(
        'source' => $this->iri,
        'id' => '',
        'start' => 0,
        'end' => 0,
      ),
    );
  }

  public function extractTitle() {
    $selector = '//h3[2]';
    $nodes = $this->execXpath($selector);
    if(strtolower($nodes->item(0)->nodeValue) == 'editorial') {
      $selector = '//h3[3]';
      $nodes = $this->execXpath($selector);
    }
    $title = $nodes->item(0)->nodeValue;
    $this->data['annotations'][] = array(
      'type' => 'hasTitle',
      'label' => 'Titolo',
      'body' => array(
        'subject' => $this->iri,
        'predicate' => 'dcterms:title',
        'object' => $title,
      ),
      'target' => array(
        'source' => $this->iri,
        'id' => $this->encodeSelector($selector),
        'start' => 0,
        'end' => strlen($title),
      ),
    );
  }

  public function extractPublicationYear() {
    $selector = '//p[1]';
    $nodes = $this->execXpath($selector);
    $regex = '/([0-9]{4})/';
    if(preg_match($regex, $nodes->item(0)->nodeValue, $results, PREG_OFFSET_CAPTURE)) {
      $year = $results[1][0];
      $year_start = $results[1][1];
      $this->data['annotations'][] = array(
        'type' => 'hasPublicationYear',
        'label' => 'Anno di pubblicazione',
        'body' => array(
          'subject' => $this->iri,
          'predicate' => 'fabio:hasPublicationYear',
          'object' => $year,
        ),
        'target' => array(
          'source' => $this->iri,
          'id' => $this->encodeSelector($selector),
          'start' => $year_start,
          'end' => $year_start+4,
        ),
      );
    }
  }

  public function extractDoi() {
    $selector = '//p[2]';
    $nodes = $this->execXpath($selector);
    $regex = '/doi:(.*)/';
    if(preg_match($regex, $nodes->item(0)->nodeValue, $results, PREG_OFFSET_CAPTURE)) {
      $doi = strip_tags($results[1][0]);
      $doi_start = $results[1][1];
      $this->data['annotations'][] = array(
        'type' => 'hasDOI',
        'label' => 'DOI',
        'body' => array(
          'subject' => $this->iri,
          'predicate' => 'prism:doi',
          'object' => $doi,
        ),
        'target' => array(
          'source' => $this->iri,
          'id' => $this->encodeSelector($selector),
          'start' => $doi_start,
          'end' => $doi_start+strlen($doi),
        ),
      );
    }
  }

  public function extractAuthor() {
    $selector = '//p[2]';
    $nodes = $this->execXpath($selector);
    $raw_authors = $this->dom->saveHTML($nodes->item(0));
      $authors_strings = explode('<br><br>', $raw_authors);
      $target_start = 0;
      //the last element is the doi, so we don't iterate over it
      for($i=0;$i<(count($authors_strings)-1);$i++) {
        $author_raw_string = explode('<br>', $authors_strings[$i]);
        $author = trim(strip_tags($author_raw_string[0]));
        $target_end = $target_start+strlen($author);
        $this->data['annotations'][] = array(
          'type' => 'hasAuthor',
          'label' => 'Autore',
          'body' => array(
            'label' => "Un autore del documento è $authors[$i].",
            'subject' => $this->iri,
            'predicate' => 'dcterms:creator',
            'resource' => array(
              'id' => $this->createPersonIri($author),
              'label' => $author,
            ),
          ),
          'target' => array(
            'source' => $this->iri,
            'id' => $this->encodeSelector($selector),
            'start' => $target_start,
            'end' => $target_end,
          ),
        );
        $target_start = $target_end+2;
      }
  }

  public function extractCitations() {
    $selector = "//p/a[@name]/..";
    $nodes = $this->execXpath($selector);
    $node_counter = 1;
    foreach($nodes as $node) {
      //DOMNode has no method to get its outer html, so we use DOMDocument::saveHTML() to get it
      $raw_citation = $this->dom->saveHTML($node);
      $cited_iri = $this->getMagazineName() . ':cited_' . $node_counter;
      $results = array();
      $regex = '/<p><a name=\"[0-9]+\">.*<\/a>(.*).*<a href=\"(.*)\">(.*)<\/a>.*([0-9]+).*/';
      if(preg_match($regex, $raw_citation, $results, PREG_OFFSET_CAPTURE)) {
        $authors = explode(', ', $results[1][0]);
        $authors_start = $results[1][1];
        $url = $results[1][0];
        $url_start = $results[1][«][1];
        $title = $results[3][0];
        $title_start = $results[3][1];
        $pub_year = $results[4][0];
        $pub_year_start = $results[3][1];
        //prepares authors in advance, as they need special treatment
        $cited_authors = array();
        for($i=0;$i<count($authors);$i++) {
          $target_end = $authors_start+strlen($authors[$i]);
          $cited_authors[] = array(
            'type' => 'hasAuthor',
            'label' => 'Autore',
            'body' => array(
              'label' => "Un autore del documento è $authors[$i].",
              'subject' => $cited_iri,
              'predicate' => 'dcterms:creator',
              'resource' => array(
                'id' => $this->createPersonIri($authors[$i]),
                'label' => $authors[$i],
              ),
            ),
            'target' => array(
              'source' => $this->iri,
              'id' => $this->encodeSelector($node->getNodePath()),
              'start' => $authors_start,
              'end' => $target_end,
            ),
          );
          $authors_start = $target_end+2;
        }
        $this->data['annotations'][] = array(
          'type' => 'cites',
          'label' => 'Questo documento cita',
          'body' => array(
            'subject' => $this->iri,
            'predicate' => 'cito:cites',
            'resource' => array(
              'id' => $cited_iri,
              'label' => $node->nodeValue,
              // annotations on the cited document here
              'annotations' => array(
                //cited document title
                0 => array(
                  'type' => 'hasTitle',
                  'label' => 'Titolo',
                  'body' => array(
                    'subject' => $cited_iri,
                    'predicate' => 'dcterms:title',
                    'object' => $title,
                  ),
                  'target' => array(
                    'source' => $this->iri,
                    'id' => $this->encodeSelector($node->getNodePath()),
                    'start' => $title_start,
                    'end' => strlen($title),
                  ),
                ),
                // cited document publication year
                1 => array(
                  'type' => 'hasPublicationYear',
                  'label' => 'Anno di pubblicazione',
                  'body' => array(
                    'subject' => $cited_iri,
                    'predicate' => 'fabio:hasPublicationYear',
                    'object' => $pub_year,
                  ),
                  'target' => array(
                    'source' => $this->iri,
                    'id' => $this->encodeSelector($node->getNodePath()),
                    'start' => $pub_year_start,
                    'end' => $pub_year_start+4,
                  ),
                ),
                //cited document url
                2 => array(
                  'type' => 'hasUrl',
                  'label' => 'Articolo disponibile all\'indirizzo',
                  'body' => array(
                    'subject' => $cited_iri,
                    'predicate' => 'fabio:hasUrl',
                    'object' => $url,
                  ),
                  'target' => array(
                    'source' => $this->iri,
                    'id' => $this->encodeSelector($node->getNodePath()),
                    'start' => $url_start,
                    'end' => $url_start+strlen($url),
                    ),
                ),
              ),
              //authors of cited document will be added later
            ),
          ),
          'target' => array(
            'source' => $this->iri,
            'id' => $this->encodeSelector($node->getNodePath()),
            'start' => 0,
            'end' => strlen($node->nodeValue),
          ),
        );
        //adding cited document authors separately, so that they are not in a separate array
        $last_annotation_index = (count($this->data['annotations'])-1);
        foreach($cited_authors as $cited_author) {
          $this->data['annotations'][$last_annotation_index]['body']['resource']['annotations'][] = $cited_author;
        }
        $node_counter++;
      }
    }
  }

  public function extractRhetoric() {
    //abstract
    $abstract_selector = '//p[4]';
    $nodes = $this->execXpath($abstract_selector);
    $abstract = $nodes->item(0)->nodeValue;
    $this->data['annotations'][] = array(
      'label' => 'Abstract',
      'type' => 'denotesRhetoric',
      'body' => array(
        'label' => 'L\'abstract dell\'articolo è ' . $abstract,
        'subject' => $abstract,
        'predicate' => 'sem:denotes',
        'object' => 'sro:Abstract',
      ),
      'target' => array(
        'id' => $this->encodeSelector($abstract_selector),
        'start' => 0,
        'end' => strlen($abstract),
      ),
    );
    //introduction
    $intro_selector = '//p[6]';
    $nodes = $this->execXpath($intro_selector);
    $intro = $nodes->item(0)->nodeValue;
    $this->data['annotations'][] = array(
      'label' => 'Introduzione',
      'type' => 'denotesRhetoric',
      'body' => array(
        'label' => 'L\'introduzione dell\'articolo è ' . $intro,
        'subject' => $abstract,
        'predicate' => 'sem:denotes',
        'object' => 'deo:Introduction',
      ),
      'target' => array(
        'id' => $this->encodeSelector($intro_selector),
        'start' => 0,
        'end' => strlen($intro),
      ),
    );
    //unfortunately sro:conclusion cannot be determined exactly due to its varying position in the markup and laking of any id referencing it
  }

}

?>
