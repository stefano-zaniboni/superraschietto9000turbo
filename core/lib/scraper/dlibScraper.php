<?php
/**
* Scraper for dlib magazine, issue 20
*/

require_once "scraperBase.php";
require_once "scraperInterface.php";

class dlibScraper extends scraperBase implements scraperInterface{
  var $url;

  public function __construct($article_url) {
    $this->loadDocument($article_url);
    $this->url = $article_url;
    $this->iri = $this->createDocumentIri($article_url);
  }

  public function parse() {
    $this->extractTitle();
    $this->extractDoi();
    $this->extractPublicationYear();
    $this->extractAuthor();
    $this->extractCitations();
    $this->extractUrl();
    $this->extractRhetoric();
    $this->setProvenance();
  }

  /**
  * Returns the magazine name
  */
  public function getMagazineName() {
    return 'dlib';
  }

  public function extractUrl() {
    $this->data['annotations'][] = array(
      'type' => 'hasUrl',
      'label' => 'Articolo disponibile all\'indirizzo',
      'body' => array(
        'subject' => $this->iri,
        'predicate' => 'fabio:hasUrl',
        'object' => $this->url,
      ),
      'target' => array(
        'source' => $this->iri,
        'id' => '',
        'start' => 0,
        'end' => 0,
      ),
    );
  }

  public function extractTitle() {
    $selector = '//h3[2]';
    $nodes = $this->execXpath($selector);
    if(strtolower($nodes->item(0)->nodeValue) == 'editorial') {
      $selector = '//h3[3]';
      $nodes = $this->execXpath($selector);
    }
    $title = $nodes->item(0)->nodeValue;
    $this->data['annotations'][] = array(
      'type' => 'hasTitle',
      'label' => 'Titolo',
      'body' => array(
        'subject' => $this->iri,
        'predicate' => 'dcterms:title',
        'object' => $title,
      ),
      'target' => array(
        'source' => $this->iri,
        'id' => $this->encodeSelector($selector),
        'start' => 0,
        'end' => strlen($title),
      ),
    );
  }

  public function extractPublicationYear() {
    $selector = '//p[1]';
    $nodes = $this->execXpath($selector);
    $regex = '/([0-9]{4})/';
    if(preg_match($regex, $nodes->item(0)->nodeValue, $results, PREG_OFFSET_CAPTURE)) {
      $year = strip_tags($results[1][0]);
      $year_start = $results[1][1];
      $this->data['annotations'][] = array(
        'type' => 'hasPublicationYear',
        'label' => 'Anno di pubblicazione',
        'body' => array(
          'subject' => $this->iri,
          'predicate' => 'fabio:hasPublicationYear',
          'object' => $year,
        ),
        'target' => array(
          'source' => $this->iri,
          'id' => $this->encodeSelector($selector),
          'start' => $year_start,
          'end' => $year_start+strlen($year),
        ),
      );
    }
  }

  public function extractDoi() {
    $selector = '//p[2]';
    $nodes = $this->execXpath($selector);
    $regex = '/doi:(.*)/';
    if(preg_match($regex, $nodes->item(0)->nodeValue, $results, PREG_OFFSET_CAPTURE)) {
      $doi = strip_tags($results[1][0]);
      $doi_start = $results[1][1];
      $this->data['annotations'][] = array(
        'type' => 'hasDOI',
        'label' => 'DOI',
        'body' => array(
          'subject' => $this->iri,
          'predicate' => 'prism:doi',
          'object' => $doi,
        ),
        'target' => array(
          'source' => $this->iri,
          'id' => $this->encodeSelector($selector),
          'start' => $doi_start,
          'end' => $doi_start+strlen($doi),
        ),
      );
    }
  }

  public function extractAuthor() {
    $selector = '//p[2]';
    $nodes = $this->execXpath($selector);
    $raw_authors = $this->dom->saveHTML($nodes->item(0));
    $authors_strings = explode('<br><br>', $raw_authors);
    $target_start = 0;
    //the last element is the doi, so we don't iterate over it
    for($i=0;$i<(count($authors_strings)-1);$i++) {
      $author_raw_string = explode('<br>', $authors_strings[$i]);
      $author = trim(strip_tags($author_raw_string[0]));
      // trim removes newline characters, so strlen($author) is unreliable for target end
      $target_end = $target_start+strlen(strip_tags($author_raw_string[0]));
      $this->data['annotations'][] = array(
        'type' => 'hasAuthor',
        'label' => 'Autore',
        'body' => array(
          'label' => "Un autore del documento è $author.",
          'subject' => $this->iri,
          'predicate' => 'dcterms:creator',
          'resource' => array(
            'id' => $this->createPersonIri($author),
            'label' => $author,
          ),
        ),
        'target' => array(
          'source' => $this->iri,
          'id' => $this->encodeSelector($selector),
          'start' => $target_start,
          'end' => $target_end,
        ),
      );
      $target_start = $target_start+strlen(strip_tags($authors_strings[$i]));
    }
  }

  public function extractCitations() {
    $selector = "//p/a[@name]/..";
    $nodes = $this->execXpath($selector);
    // since citations have a pretty varying structure, we try to scrape their fragments
    $authors_regex = '/((?:[A-Z]\. )+[a-zA-Z]+)/';
    $node_counter = 1;
    foreach($nodes as $node) {
      $annotations = array();
      $target_info = array(
        'source' => $this->iri,
        'id' => $this->encodeSelector($node->getNodePath()),
        'start' => 0,
        'end' => strlen($node->nodeValue),
      );
      //DOMNode has no method to get its outer html, so we use DOMDocument::saveHTML() to get it
      $raw_citation = $this->dom->saveHTML($node);
      $cited_iri = $this->getMagazineName() . ':cited_' . $node_counter;
      $results = array();
      // authors handling
      if(preg_match_all($authors_regex, $raw_citation, $results)) {
        foreach($results[1] as $author) {
          $annotations[] = array(
            'type' => 'hasAuthor',
            'label' => 'Autore',
            'body' => array(
              'label' => "Un autore del documento è $author.",
              'subject' => $cited_iri,
              'predicate' => 'dcterms:creator',
              'resource' => array(
                'id' => $this->createPersonIri($author),
                'label' => $author,
              ),
            ),
            'target' => $target_info,
          );
        } //end of cycle through authors
      }
      //url and title handling
      $results = array(); //reinitialize to be sure it is clean
      $url_title_regex = '/<a href="(.+)">(.+)<\/a>/';
      if(preg_match_all($url_title_regex, $raw_citation, $results)) {
        $urls = $results[1];
        $titles = $results[2];
        // we expect a title and an URL for each citation, but we cycle through them in case we get more info
        //the two arrays length is always identical, so whichever we cycle on does not make any difference
        for($i=0;$i<count($urls);$i++) {
          $url = $urls[$i];
          $annotations[] = array(
            'type' => 'hasUrl',
            'label' => 'Articolo disponibile all\'indirizzo',
            'body' => array(
              'subject' => $cited_iri,
              'predicate' => 'fabio:hasUrl',
              'object' => $url,
            ),
            'target' => $target_info,
          );
          //sometimes the title could be the same as the url, in those cases we do not show it
          $title = trim(strip_tags($titles[$i]));
          if($title != $url) {
            $annotations[] = array(
              'type' => 'hasTitle',
              'label' => 'Titolo',
              'body' => array(
                'subject' => $cited_iri,
                'predicate' => 'dcterms:title',
                'object' => $title,
              ),
              'target' => $target_info,
            );
          }
        } //end of cycle through urls and titles
      }
      //pubblication year handling
      $pub_year_regex = '/([0-9]{4})/';
      // @todo: if possible, find a better regex to avoid getting trash results
      $results = array(); //initialize to ensure that we're working with a clean array
      if(preg_match_all($pub_year_regex, $raw_citation, $results)) {
        //we expect only a pubblication year to be returned by the regex, but we cycle through the results in case we get more info
        foreach($results[1] as $pub_year) {
          //unfortunately we get some trash, so we filter it out with the following rules:
            //1. for a citation, a pubblication year in the future makes no sense
            //2. For a citation, a pubblication year prior than 1900 makes no sense
            if($pub_year>1900 && $pub_year <= date(Y)) {
              $annotations[] = array(
                'type' => 'hasPublicationYear',
                'label' => 'Anno di pubblicazione',
                'body' => array(
                  'subject' => $cited_iri,
                  'predicate' => 'fabio:hasPublicationYear',
                  'object' => $pub_year,
                ),
                'target' => $target_info,
              );
            }
        } //end of cycle through pubblication years
      }
      $this->data['annotations'][] = array(
        'type' => 'cites',
        'label' => 'Questo documento cita',
        'body' => array(
          'subject' => $this->iri,
          'predicate' => 'cito:cites',
          'resource' => array(
            'id' => $cited_iri,
            'label' => $node->nodeValue,
            // annotations on the cited document here
            'annotations' => $annotations,
          ),
        ),
        'target' => $target_info,
      );
    } //end of cycle through nodes
  }

  public function extractRhetoric() {
    //abstract
    $abstract_selector = "//p[preceding-sibling::h3[contains(text(),'Abstract')]][following-sibling::h3[contains(text(),'Introduction')]]";
    $nodes = $this->execXpath($abstract_selector);
    foreach($nodes as $node) {
      $abstract = strip_tags($node->nodeValue);
      $this->data['annotations'][] = array(
        'label' => 'Abstract',
        'type' => 'denotesRhetoric',
        'body' => array(
          'label' => 'L\'abstract dell\'articolo è ' . $abstract,
          'subject' => $abstract,
          'predicate' => 'sem:denotes',
          'object' => 'sro:Abstract',
        ),
        'target' => array(
          'id' => $this->encodeSelector($node->getNodePath()),
          'start' => 0,
          'end' => strlen($abstract),
        ),
      );
    }
    //introduction
    $intro_selector = "//p[preceding-sibling::h3[contains(text(),'Introduction')]][following-sibling::h3[contains(text(),'2')]]";
    $nodes = $this->execXpath($intro_selector);
    foreach($nodes as $node) {
      $intro = strip_tags($node->nodeValue);
      $this->data['annotations'][] = array(
        'label' => 'Introduzione',
        'type' => 'denotesRhetoric',
        'body' => array(
          'label' => 'L\'introduzione dell\'articolo è ' . $intro,
          'subject' => $intro,
          'predicate' => 'sem:denotes',
          'object' => 'deo:Introduction',
        ),
        'target' => array(
          'id' => $this->encodeSelector($node->getNodePath()),
          'start' => 0,
          'end' => strlen($intro),
        ),
      );
    }
    //conclusion
    $conclusion_selector = "//p[preceding-sibling::h3[contains(text(),'Conclusion')]][following-sibling::h3[contains(text(),'Notes')]]";
    $nodes = $this->execXpath($conclusion_selector);
    foreach($nodes as $node) {
      $conclusion = strip_tags($node->nodeValue);
      $this->data['annotations'][] = array(
        'label' => 'Conclusione',
        'type' => 'denotesRhetoric',
        'body' => array(
          'label' => 'La conclusione dell\'articolo è ' . $conclusion,
          'subject' => $conclusion,
          'predicate' => 'sem:denotes',
          'object' => 'deo:Conclusion',
        ),
        'target' => array(
          'id' => $this->encodeSelector($node->getNodePath()),
          'start' => 0,
          'end' => strlen($intro),
        ),
      );
    }
  }

}

?>
