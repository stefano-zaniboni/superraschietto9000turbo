<?php
/*@@@@N.B GOUTTE SI INCAZZA SE VEDE TBODY NELLE ESPRESSIONI XPATH!*/
require_once '../vendor/goutte.phar';
use Goutte\Client;

if(isset($_GET['url'])) {
  $url = $_GET['url'];
}
//client used to send requests to a website and returns a crawler object
$client = new Client();
$client->getClient()->setDefaultOption('config/curl/'.CURLOPT_SSL_VERIFYHOST, FALSE); //codice per accettare anche https
$client->getClient()->setDefaultOption('config/curl/'.CURLOPT_SSL_VERIFYPEER, FALSE);
$crawler = $client->request('GET', $url);

//Here I declared a client object, and called “Request()” to simulate browser requesting the url “http://zrashwani.com” using “GET” http method.
//Request() method returns an object of type Symfony\Component\DomCrawler\Crawler, than can be used to select elements from the fetched html response. But before processing the document, let’s ensure that this URL is a valid link, which means it returned a response code (200)
$status_code = $client->getResponse()->getStatus();
if($status_code==200){
    //process the documents
    //$result = $crawler->filterXPath('html/body')->html(); //ho usato questo per provare e funzionava
	
    if(strpos($url, 'rivista-statistica.unibo.it') !== false) {
    	//filtro definitivo per statistica
      	$result = $crawler->filterXPath('html/body/div/div[2]/div[2]/div[3]')->html(); 
      	//$result=cancella_caratteri_fastidiosi($result);
      	echo $result;
    }
    else { 
	    	if(strpos($url, 'dlib.org') !== false) {
	    		//filtro definitivo per dlib
	     	 	$result = $crawler->filterXPath('html/body/form[1]/table[3]/tr/td/table[5]/tr/td/table[1]/tr/td[2]')->html(); //l'indice 1 e' proprio il primo!
	     	 	//$result=cancella_caratteri_fastidiosi($result);
	     	 	echo $result;
	      	}
	  	else { 
		  		if(strpos($url, 'almatourism.unibo.it') !== false) {
		  		//filtro definitivo per almatourism
	      		$result = $crawler->filterXPath('html/body/div/div[2]/div[2]/div[3]')->html();
	      		//$result=cancella_caratteri_fastidiosi($result); 
	      		echo $result;
		  		}
	  		else { 
	  				if(strpos($url, 'antropologiaeteatro.unibo.it') !== false) {
	  				//filtro definitivo per antropologia
		  			$result = $crawler->filterXPath('html/body/div/div[2]/div[2]/div[3]')->html();
		  			//$result=cancella_caratteri_fastidiosi($result); 
		 	 		echo $result;
	  				}
	  			else {
          			//definire il caso di documento generico
          			$result = $crawler->filterXPath('html/body')->html();
          			//$result=cancella_caratteri_fastidiosi($result);
          			echo $result;
	  			}
	  		}
	  	}
	  }
}
else {
	//in case of error
	echo "HTTP/1.0 400 Bad Request";
}

function cancella_caratteri_fastidiosi($conv) {
	$string = htmlentities($conv, null, 'utf-8');
	$conv = str_replace("&nbsp;", "", $string);
	$conv = html_entity_decode($conv);
	$conv=trim($conv);
	return($conv);
}

?>
