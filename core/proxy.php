<?php
require_once "lib/scraper/const.inc";
require_once "lib/scraper/scraperBase.php";

class proxy extends scraperBase{
  var $url;
  
  public function __construct($article_url) {
    $this->loadDocument($article_url);
    $this->url = $article_url;
  }
  
  /**
    * Extract the page content by using an xpath
    * @param $xpath: the xpath to use
    * Warning: make sure the provided xpath returns only one element. Selected items other than the first one are ignored by this function.
    */
  private function extractContentUsingXpath($xpath) {
    $content_nodes = $this->execXpath($xpath);
    // we want the inner html, so we need to iterate through the node children
        $xpath_node = $content_nodes->item(0);
        $content = '';
        foreach($xpath_node->childNodes as $node) {
              $content .= $this->dom->saveHTML($node);
        }
    return $content;
  }
  
  /**
    * Returns the content of the requested page
    */
  public function getPageContent() {
    if(strpos($this->url, 'almatourism.unibo.it') !== false) {
      $page_content = $this->extractContentUsingXpath(almatourism_base_xpath);
    }
    else if(strpos($this->url, 'antropologiaeteatro.unibo.it') !== false) {
      $page_content = $this->extractContentUsingXpath(antropologia_base_xpath);
    }
    else if(strpos($this->url, 'dlib.org') !== false) {
      $page_content = $this->extractContentUsingXpath(dlib_base_xpath);
      }
          else if(strpos($this->url, 'rivista-statistica.unibo.it') !== false) {
      $page_content = $this->extractContentUsingXpath(statistica_base_xpath);
    }
    else {
      $page_content = $this->extractContentUsingXpath(generic_base_xpath);
    }
    //some sanitization
    //$page_content = preg_replace('/\\r|\\n|\\t/', '', $page_content);
    return html_entity_decode($page_content);
  }

}

if(isset($_GET['url'])) {
  $url = $_GET['url'];
  $proxy = new proxy($url);
  echo $proxy->getPageContent();
}
else {
  header("HTTP/1.0 400 Bad Request");
}


?>

